/*
* Developed by Alexander Hart
 * Plant Computational Genomics Lab
 * University of Connecticut
 *
 * For information, contact Alexander Hart at:
 *     entap.dev@gmail.com
 *
 * Copyright 2017-2025, Alexander Hart, Dr. Jill Wegrzyn
 *
 * This file is part of EnTAP.
 *
 * EnTAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnTAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnTAP.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "NCBIDatabase.h"

#include <regex>

#include "../QueryAlignment.h"
#include <thread>

const std::string NCBIDatabase::NCBI_UNKNOWN_GENE_ID="UNKNOWN_GENE_ID";


NCBIDatabase::~NCBIDatabase()
{
    mProcessRemaining = true;
    mContinueProcessing = false;
    // Just cleanup and force threads to finish
    for (std::thread& t : mThreadPool) {
        if (t.joinable()) {
            t.join();
        }
    }
    delete mpNCBIEntrez;
}

void NCBIDatabase::wait_for_completion() {
    mProcessRemaining = true;

    if (mAvailableThreadNum == 0) {
        // If object was created with no available threads (i.e. we're working with 1)
        //  we want to hijack the main thread to complete this
        main_loop();
    } else {
        for (std::thread& t : mThreadPool) {
            if (t.joinable()) {
                t.join();
            }
        }
    }
}

NCBIDatabase::NCBIDatabase(FileSystem *pfile_system, size_t thread_num, std::string user_api_key)
{
    mContinueProcessing = true;
    mProcessRemaining = false;
    mpNCBIEntrez = new NCBIEntrez(pfile_system, user_api_key);
    mNCBIAccessionType = NCBI_ACCESSION_ENTREZ;
    mNCBIResults = std::unordered_map<std::string, NCBIData>();
    mUserAPIKey = user_api_key;
    mAvailableThreadNum = thread_num;
    for (size_t i = 0; i < thread_num; ++i) {
        mThreadPool.emplace_back(&NCBIDatabase::main_loop, this);
    }
}

NCBIData NCBIDatabase::get_ncbi_data(const std::string& ncbi_subject_id)const
{
    auto pos = mNCBIResults.find(ncbi_subject_id);
    if (pos != mNCBIResults.end()) {
        return pos->second;
    } else {
        return {};
    }
}

void NCBIDatabase::add_ncbi_query(const std::string& ncbi_subject_id) {
    std::lock_guard<std::mutex> lock(mDataMutex);
    mProcessingQueue.emplace(ncbi_subject_id);
}

bool NCBIDatabase::is_ncbi_match(const std::string& ncbi_sseqid) {
    const std::string NCBI_REGEX_MATCH = "AC_|NC_|NG_|NT_|NW_|NZ_|NM_|NR_|XM_|XR_|AP_|NP_|YP_|XP_|WP_";
    std::smatch match;
    /*
     *  This may create false positives since some Uniprot accessions will match
     *      in NCBI database
    if (mpNCBIEntrez != nullptr) {
        NCBIEntrez::EntrezInput entrez_input;
        entrez_input.term=ncbi_sseqid;
        return mpNCBIEntrez->entrez_has_hits(entrez_input);
    }
    */
    if (ncbi_sseqid.empty()) return false;
    return std::regex_search(ncbi_sseqid, match, std::regex(NCBI_REGEX_MATCH));
}

void NCBIDatabase::process_queue()  {
    vect_str_t ncbi_accessions;

    if (mProcessingQueue.empty()) return;
    {
        std::lock_guard<std::mutex> lock(mQueueMutex);
        if ((mProcessingQueue.size() >= ENTREZ_QUEUE_SIZE) || (mProcessRemaining))
        {
            for (int i=0; i<ENTREZ_QUEUE_SIZE && !mProcessingQueue.empty(); i++) {
                ncbi_accessions.push_back(mProcessingQueue.front());
                mProcessingQueue.pop();
            }
        } else {
            return;
        }
    }

    switch (mNCBIAccessionType) {

        case NCBI_ACCESSION_ENTREZ: {
            if (mpNCBIEntrez == nullptr) return;
            NCBIEntrez::EntrezResults entrez_results;
            NCBIEntrez::EntrezInput entrez_input;
            entrez_input.database = NCBIEntrez::NCBI_DATABASE_PROTEIN;
            entrez_input.rettype = NCBIEntrez::NCBI_ENTREZ_RETTYPE_GP;
            entrez_input.data_types = {NCBIEntrez::ENTREZ_DATA_GENEID};
            entrez_input.uid_list = ncbi_accessions;

            for (int i=0; i<NCBI_DATABASE_RETRIES; i++) {
                if (mpNCBIEntrez->entrez_fetch(entrez_input, entrez_results)) {
                    std::lock_guard<std::mutex> lock(mDataMutex);
                    for (const auto &pair : entrez_results.entrez_results) {
                        mNCBIResults.emplace(pair.first, pair.second);
                    }
                    if (i>0) FS_dprint("Success NCBI_ACCESSION_ENTREZ after retrying");
                    break;  // Success break out of retry loop
                } else {
                    FS_dprint("Retrying NCBI_ACCESSION_ENTREZ: " + std::to_string(i));
                    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                }
            }
            break;
        }

        case NCBI_ACCESSION_API:
        default:
            break;
    }
}

void NCBIDatabase::main_loop() {
    while(mContinueProcessing) {
        process_queue();
        if (mProcessRemaining && mProcessingQueue.empty()) mContinueProcessing = false;
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}