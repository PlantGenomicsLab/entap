/*
 * Developed by Alexander Hart
 * Plant Computational Genomics Lab
 * University of Connecticut
 *
 * For information, contact Alexander Hart at:
 *     entap.dev@gmail.com
 *
 * Copyright 2017-2025, Alexander Hart, Dr. Jill Wegrzyn
 *
 * This file is part of EnTAP.
 *
 * EnTAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnTAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnTAP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ENTAP_NCBIENTREZ_H
#define ENTAP_NCBIENTREZ_H

#ifdef ENABLE_TESTING
#include <gtest/gtest_prod.h>
#endif
#include "NCBIDatabase.h"
#include "../common.h"
#include "../FileSystem.h"

class NCBIEntrez {

public:
    // Data we want to access from the NCBI database through Entrez
    typedef enum
    {
        ENTREZ_DATA_NULL=0,
        ENTREZ_DATA_GENEID=1  // GeneID, ex: '106664428'
    } ENTREZ_DATA_TYPES;

    // Required input for an Entrez Search
    struct EntrezInput {
        std::string database;
        std::string retmode;
        std::string rettype;
        std::string term;
        vect_str_t  uid_list;
        std::vector<ENTREZ_DATA_TYPES> data_types;
    };

    struct EntrezResults {
        std::string count;
        vect_str_t uid_list;
        NCBIDataResults_t entrez_results;
    };

    // Entrez DATABASE types
    static const std::string NCBI_DATABASE_TAXONOMY;
    static const std::string NCBI_DATABASE_PROTEIN;

    // Entrez RETTYPE types
    static const std::string NCBI_ENTREZ_RETTYPE_XML;
    static const std::string NCBI_ENTREZ_RETTYPE_GP;  // Genpep flat file
    static const std::string NCBI_ENTREZ_RETTYPE_FASTA;

    // Entrez RETMODE types
    static const std::string NCBI_ENTREZ_RETMODE_TEXT;

    NCBIEntrez(FileSystem *fileSystem, std::string api_key);
    ~NCBIEntrez();
    bool entrez_has_hits(EntrezInput &entrezInput);
    bool entrez_search(EntrezInput &entrezInput, EntrezResults &entrezResults);
    bool entrez_fetch(EntrezInput &entrezInput, EntrezResults &entrezResults);
    bool verify_api_key();

    // NCBI limits access to database fetches to 3 accessions per 1 second
    //  we will add a significant buffer here to never exceed with mulithreaded pulling
    static constexpr uint32 NCBI_ENTREZ_API_TIMEOUT = 2000;            // (ms) timeout of allowed lookups
    static constexpr uint32 NCBI_ENTREZ_NO_API_KEY_ACCESSION_PER_TIMEOUT  = 3; // number of accessions per timeout allowed by NCBI
    static constexpr uint32 NCBI_ENTREZ_YES_API_KEY_ACCESSION_PER_TIMEOUT = 9; // accessions/timeout with API key - 1 for buffer

protected:
#ifdef ENABLE_TESTING
    class NCBIEntrezTests;
    FRIEND_TEST(NCBIEntrez, APIAccessLimit);
#endif

    typedef enum {
        ENTREZ_SEARCH,
        ENTREZ_SUMMARY,
        ENTREZ_FETCH
    } ENTREZ_TYPES;

    // Entrez unique identifiers
    const std::string ENTREZ_TOOL_IDENTIFIER = "entap";
    const std::string ENTREZ_EMAIL_IDENTIFIER = "entap.dev@gmail.com";

    // Entrez Base URLs
    const std::string ESEARCH_BASE_URL = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?";
    const std::string EPOST_BASE_URL   = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/epost.fcgi?";
    const std::string EFETCH_BASE_URL  = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?";
    const std::string ESUMMARY_BASE_URL= "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?";

    // Entrez commands
    const std::string NCBI_ENTREZ_RETTYPE = "rettype=";
    const std::string NCBI_ENTREZ_RETMODE = "retmode=";
    const std::string NCBI_ENTREZ_DATABASE= "db=";
    const std::string NCBI_ENTREZ_UID     = "id=";
    const std::string NCBI_ENTREZ_TERM    = "term=";
    const std::string NCBI_ENTREZ_API_KEY = "api_key=";
    const std::string NCBI_ENTREZ_EMAIL_KEY = "email=";
    const std::string NCBI_ENTREZ_TOOL_KEY  = "tool=";

    // Rettypes
    const std::string NCBI_ENTREZ_RETTYPE_COUNT = "count";

    // XML
    const std::string NCBI_ENTREZ_UID_XML_START = "<Id>";
    const std::string NCBI_ENTREZ_UID_XML_END   = "</Id>";
    const std::string NCBI_ENTREZ_COUNT_XML_START= "<Count>";
    const std::string NCBI_ENTREZ_COUNT_XML_END  = "</Count>";

    const std::string NCBI_ENTREZ_COUNT_ZERO     = "0";

    FileSystem *mpFileSystem;
    std::string mUserAPIKey;
    uint32      mAllowedAccessionsPerSecond;
    std::mutex mTimeMutex;
    uint16 mAccessionPerTimeoutCt; // count of accesions per timeout
    std::chrono::time_point<std::chrono::system_clock> mTimeSinceAccession;

    bool allowed_to_access_api();
    std::string generate_query(EntrezInput &entrezInput, ENTREZ_TYPES type);
    void process_term(std::string& term);
    bool parse_ncbi_gp_file(EntrezInput &entrezInput, EntrezResults &entrezResults, std::string &output_file);
};

#endif //ENTAP_NCBIENTREZ_H
