/*
 *
 * Developed by Alexander Hart
 * Plant Computational Genomics Lab
 * University of Connecticut
 *
 * For information, contact Alexander Hart at:
 *     entap.dev@gmail.com
 *
 * Copyright 2017-2025, Alexander Hart, Dr. Jill Wegrzyn
 *
 * This file is part of EnTAP.
 *
 * EnTAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnTAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnTAP.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "FastaDatabase.h"
#include "../FileSystem.h"

// WARNING this table should always match order of FASTA_DATABASE_TYPE enum in FastaDatabase.h
const FastaDatabase::FastaDatabaseInfo FastaDatabase::FASTA_DATABASE_INFO[] = {
    {FASTA_DATABASE_NCBI_ARCHAEA, "ftp://ftp.ncbi.nlm.nih.gov/refseq/release/archaea", "refseq_archaea"},
    {FASTA_DATABASE_NCBI_BACTERIA, "ftp://ftp.ncbi.nlm.nih.gov/refseq/release/bacteria/", "refseq_bacteria"},
    {FASTA_DATABASE_NCBI_COMPLETE, "ftp://ftp.ncbi.nlm.nih.gov/refseq/release/complete/", "refseq_complete"},
    {FASTA_DATABASE_NCBI_FUNGI, "ftp://ftp.ncbi.nlm.nih.gov/refseq/release/fungi/", "refseq_fungi"},
    {FASTA_DATABASE_NCBI_INVERTEBRATE, "ftp://ftp.ncbi.nlm.nih.gov/refseq/release/invertebrate/", "refseq_invertebrate"},
    {FASTA_DATABASE_NCBI_OTHER, "ftp://ftp.ncbi.nlm.nih.gov/refseq/release/other/", "refseq_other"},
    {FASTA_DATABASE_NCBI_PLANT, "ftp://ftp.ncbi.nlm.nih.gov/refseq/release/plant/", "refseq_plant"},
    {FASTA_DATABASE_NCBI_PROTOZOA, "ftp://ftp.ncbi.nlm.nih.gov/refseq/release/protozoa/", "refseq_protozoa"},
    {FASTA_DATABASE_NCBI_VERTEBRATE_MAMMALIAN, "ftp://ftp.ncbi.nlm.nih.gov/refseq/release/vertebrate_mammalian/", "refseq_vertebrate_mammalian"},
    {FASTA_DATABASE_NCBI_VERTEBRATE_OTHER, "ftp://ftp.ncbi.nlm.nih.gov/refseq/release/vertebrate_other/", "refseq_vertebrate_other"},
    {FASTA_DATABASE_NCBI_NR, "ftp://ftp.ncbi.nlm.nih.gov/blast/db/FASTA/nr.gz", "ncbi_nr"},
    {FASTA_DATABASE_UNIPROT_SPROT, "ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.fasta.gz", "uniprot_sprot"},
    {FASTA_DATABASE_UNIPROT_TREMBL, "ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_trembl.fasta.gz", "uniprot_trembl"},
    {FASTA_DATABASE_UNKNOWN, "", ""},
};

FastaDatabase::FastaDatabase(FASTA_DATABASE_FILE_FORMAT file_format, FASTA_DATABASE_TYPE database_type, const std::string& database_path,
 FileSystem *file_system) {
    mDatabaseType = database_type;
    mDatabaseFileFormat = file_format;
    mpFilesystem = file_system;
    mDatabaseFilePath = database_path;
}

FastaDatabase::FASTA_DATABASE_TYPE FastaDatabase::is_database_type_match(std::string user_input) {
    LOWERCASE(user_input);
    for (const FastaDatabaseInfo &database_info : FASTA_DATABASE_INFO) {
        if (database_info.user_input_str == user_input) {
           return database_info.database_type;
        }
    }
    return FASTA_DATABASE_UNKNOWN;
}

FastaDatabase::FASTA_DATABASE_FILE_FORMAT FastaDatabase::get_mDatabaseFileFormat() const {
    return mDatabaseFileFormat;
}

std::string FastaDatabase::get_database_user_string() const {
    return FASTA_DATABASE_INFO[mDatabaseType].user_input_str;
}

std::string FastaDatabase::get_mDatabaseFilePath() const {
    return mDatabaseFilePath;
}

// Same as AbstractSimilaritySearch function, will change and consolidate duplication
std::string FastaDatabase::get_species(const std::string &title) {
    std::string species;


#ifdef USE_BOOST
    boost::smatch match;

    boost::regex ncbi_exp(_NCBI_REGEX);
    boost::regex uniprot_exp(_UNIPROT_REGEX);

    if (boost::regex_search(title,match,uniprot_exp)) {
        species = std::string(match[1].first, match[1].second);
    } else {
        if (boost::regex_search(title, match, ncbi_exp))
            species = std::string(match[1].first, match[1].second);
    }
#else   // Use std c++ libs
    // GCC 4.8.x does NOT fully support std regex and have bugs, full...bugless.. starts at 5.x
#if 0
    std::smatch match;

    // Check if UniProt match
    if (std::regex_search(title, match, std::regex(UNIPROT_REGEX)) && match.size() > 1) {
        species = std::string(match[1].first, match[1].second);
    } else {
        // Not a UniProt match, check NCBI standard
        if (std::regex_search(title, match, std::regex(NCBI_REGEX)) && match.size() > 1) {
            species = std::string(match[1].first, match[1].second);
        }
    }
#endif
    uint64 ind1, ind2;
    ind1 = title.find("OS=");

    // if UniProt type format for species EX: OS=Homo sapiens
    if (ind1 != std::string::npos) {
        // Yes,
        ind2 = title.find('=', ind1 + 3);
        if (ind2 != std::string::npos && (ind2 - ind1) > 6) {
            species = title.substr(ind1 + 3, ind2 - ind1 - 6);
        }

    } else {
        // No, possibly NCBI format. EX: [homo sapiens]
        ind1 = title.find_last_of('[');
        ind2 = title.find_last_of(']');
        if (ind1 != std::string::npos && ind2 != std::string::npos) {
            species = title.substr(ind1 + 1, (ind2 - ind1 - 1));
        }
    }
#endif

    // Double bracket fix
    if (species[0] == '[') species = species.substr(1);
    if (species[species.length()-1] == ']') species = species.substr(0,species.length()-1);

    return species;
}

bool FastaDatabase::download_configure_database(std::string outpath, std::string &out_err) const {
    TerminalData terminal_data;
    if (outpath.empty()) return false;
    if (mDatabaseFileFormat != FASTA_DATABASE_FILE_USER_CONFIG) return false;
    switch (mDatabaseType) {
        case FASTA_DATABASE_NCBI_ARCHAEA:
        case FASTA_DATABASE_NCBI_BACTERIA:
        case FASTA_DATABASE_NCBI_COMPLETE:
        case FASTA_DATABASE_NCBI_FUNGI:
        case FASTA_DATABASE_NCBI_INVERTEBRATE:
        case FASTA_DATABASE_NCBI_OTHER:
        case FASTA_DATABASE_NCBI_PLANT:
        case FASTA_DATABASE_NCBI_PROTOZOA:
        case FASTA_DATABASE_NCBI_VERTEBRATE_MAMMALIAN:
        case FASTA_DATABASE_NCBI_VERTEBRATE_OTHER: {
            // For the above NCBI Refseq databases, they are split between many files with a specific suffix
            // Download all of the relevant files
            std::string temp_output_dir = PATHS(mpFilesystem->get_temp_outdir(), "fasta_database");
            mpFilesystem->delete_dir(temp_output_dir);
            FileSystem::create_dir(temp_output_dir);
            terminal_data.command = "wget -r -A '*.protein.faa.gz' " + FASTA_DATABASE_INFO[mDatabaseType].ftp_path +
                " -P " + temp_output_dir + " --no-host-directories --no-directories";
            terminal_data.print_files = false;
            terminal_data.suppress_logging = true;
            terminal_data.suppress_std_err = true;
            if (TC_execute_cmd(terminal_data) != 0) {
                out_err = terminal_data.err_stream;
                mpFilesystem->delete_dir(temp_output_dir);
                return false;
            }

            // Unzip all of the files
            terminal_data = {};
            terminal_data.command = "gunzip " + temp_output_dir + "/*.gz";
            terminal_data.print_files = false;
            terminal_data.suppress_logging = true;
            terminal_data.suppress_std_err = true;
            if (TC_execute_cmd(terminal_data) != 0) {
                out_err = terminal_data.err_stream;
                mpFilesystem->delete_dir(temp_output_dir);
                return false;
            }

            // Concatenate all files together
            terminal_data = {};
            terminal_data.command = "cat " + temp_output_dir + "/*.faa > " + outpath;
            terminal_data.print_files = false;
            terminal_data.suppress_logging = true;
            terminal_data.suppress_std_err = true;
            if (TC_execute_cmd(terminal_data) != 0) {
                out_err = terminal_data.err_stream;
                mpFilesystem->delete_dir(temp_output_dir);
                return false;
            }

            // File ready to be configured by DIAMOND
            break;
        }

        // NR and Uniprot databases are downloaded through FTP then decompressed
        case FASTA_DATABASE_NCBI_NR:
        case FASTA_DATABASE_UNIPROT_SPROT:
        case FASTA_DATABASE_UNIPROT_TREMBL: {
            // Download Uniprot database, file is downloaded in .gz format
            std::string temp_out_file = mpFilesystem->get_temp_file();
            if (!mpFilesystem->download_ftp_file(FASTA_DATABASE_INFO[mDatabaseType].ftp_path,
                                                temp_out_file, false)) {
                out_err = "ERROR unable to download FTP file from address: " + FASTA_DATABASE_INFO[mDatabaseType].ftp_path;
                return false;
            }

            // Unzip file
            if (!mpFilesystem->decompress_file(temp_out_file, outpath, FileSystem::ENT_FILE_GZ)) {
                out_err = "ERROR unable to decompress file at: " + temp_out_file;
                return false;
            }
            // file ready to be configured by DIAMOND
            break;
        }

        default:
            return false;
    }
    return true;
}

