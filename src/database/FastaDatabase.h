/*
 *
 * Developed by Alexander Hart
 * Plant Computational Genomics Lab
 * University of Connecticut
 *
 * For information, contact Alexander Hart at:
 *     entap.dev@gmail.com
 *
 * Copyright 2017-2025, Alexander Hart, Dr. Jill Wegrzyn
 *
 * This file is part of EnTAP.
 *
 * EnTAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnTAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnTAP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FASTADATABASE_H
#define FASTADATABASE_H


#include "../common.h"

class FileSystem;

class FastaDatabase {

public:
 typedef enum {
  FASTA_DATABASE_FILE_TEXT,
  FASTA_DATABASE_FILE_DIAMOND,
  FASTA_DATABASE_FILE_USER_CONFIG,
  FASTA_DATABASE_FILE_UNKNOWN
 } FASTA_DATABASE_FILE_FORMAT;

 // WARNING this should always match order of FASTA_DATABASE_INFO in FastaDatabase.cpp
 typedef enum {
    FASTA_DATABASE_NCBI_ARCHAEA=0,
    FASTA_DATABASE_NCBI_BACTERIA,
    FASTA_DATABASE_NCBI_COMPLETE,
    FASTA_DATABASE_NCBI_FUNGI,
    FASTA_DATABASE_NCBI_INVERTEBRATE,
    FASTA_DATABASE_NCBI_OTHER,
    FASTA_DATABASE_NCBI_PLANT,
    FASTA_DATABASE_NCBI_PROTOZOA,
    FASTA_DATABASE_NCBI_VERTEBRATE_MAMMALIAN,
    FASTA_DATABASE_NCBI_VERTEBRATE_OTHER,
    FASTA_DATABASE_NCBI_NR,
    FASTA_DATABASE_UNIPROT_SPROT,
    FASTA_DATABASE_UNIPROT_TREMBL,
    FASTA_DATABASE_UNKNOWN
 } FASTA_DATABASE_TYPE;
 FastaDatabase(FASTA_DATABASE_FILE_FORMAT file_format, FASTA_DATABASE_TYPE database_type, const std::string &database_path,
  FileSystem *file_system);
 static FASTA_DATABASE_TYPE is_database_type_match(std::string user_input);
 FASTA_DATABASE_FILE_FORMAT get_mDatabaseFileFormat() const;
 std::string get_database_user_string() const;
 std::string get_mDatabaseFilePath() const;
 static std::string get_species(const std::string &title);
 bool download_configure_database( std::string outpath, std::string &out_err) const;


private:
 struct FastaDatabaseInfo {
  FASTA_DATABASE_TYPE database_type;
  std::string ftp_path;
  std::string user_input_str;
 };
 static const FastaDatabaseInfo FASTA_DATABASE_INFO[];
 FASTA_DATABASE_TYPE mDatabaseType;
 FASTA_DATABASE_FILE_FORMAT mDatabaseFileFormat;
 std::string mDatabaseFilePath;
 FileSystem *mpFilesystem;
};



#endif //FASTADATABASE_H
