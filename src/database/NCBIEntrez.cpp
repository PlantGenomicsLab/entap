/*
 * Developed by Alexander Hart
 * Plant Computational Genomics Lab
 * University of Connecticut
 *
 * For information, contact Alexander Hart at:
 *     entap.dev@gmail.com
 *
 * Copyright 2017-2025, Alexander Hart, Dr. Jill Wegrzyn
 *
 * This file is part of EnTAP.
 *
 * EnTAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnTAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnTAP.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "NCBIEntrez.h"
#include <regex>
#include "NCBIDatabase.h"

const std::string NCBIEntrez::NCBI_DATABASE_TAXONOMY = "taxonomy";
const std::string NCBIEntrez::NCBI_DATABASE_PROTEIN = "protein";

const std::string NCBIEntrez::NCBI_ENTREZ_RETMODE_TEXT = "text";

const std::string NCBIEntrez::NCBI_ENTREZ_RETTYPE_XML  = "xml";
const std::string NCBIEntrez::NCBI_ENTREZ_RETTYPE_GP  = "gp";
const std::string NCBIEntrez::NCBI_ENTREZ_RETTYPE_FASTA = "fasta";

NCBIEntrez::NCBIEntrez(FileSystem *fileSystem, std::string api_key) {
    mpFileSystem = fileSystem;
    mUserAPIKey = api_key; // NOTE assume valid if not empty, should have been checked already
    if (mUserAPIKey.empty()) {
        mAllowedAccessionsPerSecond = NCBI_ENTREZ_NO_API_KEY_ACCESSION_PER_TIMEOUT;
    } else {
        mAllowedAccessionsPerSecond = NCBI_ENTREZ_YES_API_KEY_ACCESSION_PER_TIMEOUT;
    }
    mTimeSinceAccession = std::chrono::system_clock::time_point();
    mAccessionPerTimeoutCt = 0;
}

NCBIEntrez::~NCBIEntrez() = default;

bool NCBIEntrez::entrez_has_hits(EntrezInput &entrezInput) {
    bool ret = false;
    std::string query;
    std::string output_file;
    std::string line;
    uint64 ind1;
    uint64 ind2;
    EntrezResults entrezResults;

    entrezInput.rettype = NCBI_ENTREZ_RETTYPE_COUNT;
    query = generate_query(entrezInput, ENTREZ_SEARCH);

    allowed_to_access_api();

    ret = mpFileSystem->download_url_data(query, output_file, false);

    // if successful download, parse
    if (ret) {
        if (mpFileSystem->file_exists(output_file)) {
            std::ifstream infile(output_file);
            while (std::getline(infile, line)) {
                // Look for count
                if (line.find(NCBI_ENTREZ_COUNT_XML_START) != std::string::npos) {
                    ind1 = line.find(NCBI_ENTREZ_COUNT_XML_START);
                    ind2 = line.find(NCBI_ENTREZ_COUNT_XML_END);
                    entrezResults.count = line.substr(ind1 + NCBI_ENTREZ_COUNT_XML_START.size(),ind2 - ind1 - NCBI_ENTREZ_COUNT_XML_START.size());
                    break;  // BREAK out of loop once we find count
                }
            }
            infile.close();
            mpFileSystem->delete_file(output_file, false);
        } else {
            ret = false;
        }
    }
    return entrezResults.count != NCBI_ENTREZ_COUNT_ZERO;
}

bool NCBIEntrez::entrez_search(NCBIEntrez::EntrezInput &entrezInput, NCBIEntrez::EntrezResults &entrezResults) {
    bool ret=false;
    std::string output_file;
    std::string line;
    std::string temp;
    std::string query = generate_query(entrezInput, ENTREZ_SEARCH);
    uint64 ind1;
    uint64 ind2;

    allowed_to_access_api();

    // Download data
    ret = mpFileSystem->download_url_data(query, output_file, false);

    // if successful download, parse
    if (ret) {
        if (mpFileSystem->file_exists(output_file)) {
            std::ifstream infile(output_file);
            while (std::getline(infile, line)) {

                // Look for ID
                if (line.find(NCBI_ENTREZ_UID_XML_START) != std::string::npos) {
                    ind1 = line.find(NCBI_ENTREZ_UID_XML_START);
                    ind2 = line.find(NCBI_ENTREZ_UID_XML_END);
                    temp = line.substr(ind1 + NCBI_ENTREZ_UID_XML_START.size(),ind2 - ind1 - NCBI_ENTREZ_UID_XML_START.size());
                    entrezResults.uid_list.push_back(temp);
                }

                // Look for count
                if (line.find(NCBI_ENTREZ_COUNT_XML_START) != std::string::npos) {
                    ind1 = line.find(NCBI_ENTREZ_COUNT_XML_START);
                    ind2 = line.find(NCBI_ENTREZ_COUNT_XML_END);
                    entrezResults.count = line.substr(ind1 + NCBI_ENTREZ_COUNT_XML_START.size(),ind2 - ind1 - NCBI_ENTREZ_COUNT_XML_START.size());
                }
            }
            infile.close();
        } else {
            ret = false;
        }
    }
    return ret;
}

bool NCBIEntrez::entrez_fetch(EntrezInput& entrezInput, EntrezResults& entrezResults) {
    std::string output_file;
    std::string line;           // line read from file downloaded from NCBI

    if (entrezInput.uid_list.empty()) return false;
    std::string query = generate_query(entrezInput, ENTREZ_FETCH);
    if (query.empty()) return false;

    allowed_to_access_api();

    // TODO will need to include CURL in project, downloading to file now to test
    // Download data
    bool ret = mpFileSystem->download_url_data(query, output_file, false);
    if (ret && mpFileSystem->file_exists(output_file))
    {
        // File successfully downloaded, parse
        if (entrezInput.rettype == NCBI_ENTREZ_RETTYPE_GP) {
            ret = parse_ncbi_gp_file(entrezInput, entrezResults, output_file);
            mpFileSystem->delete_file(output_file, false);
            return ret;
        } else {
            return false;
        }
    } else {
        // FS_dprint("ERROR ENTREZ unable to download file with query: " + query);
        return false;
    }
}

bool NCBIEntrez::verify_api_key() {
    EntrezInput entrez_input;
    entrez_input.database = NCBI_DATABASE_PROTEIN;
    entrez_input.rettype = NCBI_ENTREZ_RETTYPE_FASTA;
    entrez_input.uid_list = vect_str_t{"XP_022194947.2"};

    std::string query = generate_query(entrez_input, ENTREZ_FETCH);
    std::string temp_file;
    allowed_to_access_api();
    // Attempt to pull random FASTA data to see if api key works
    bool success = mpFileSystem->download_url_data(query, temp_file, false);
    if (success) {
        // Successfully pulled data, check if it contains "API key invalid" string from NCBI
        if (FileSystem::file_exists(temp_file)) {
            std::ifstream infile(temp_file);
            std::string line;
            while (std::getline(infile, line)) {
                if (line.empty()) continue;
                if (line.find("API key invalid") != std::string::npos) return false;
            }
            infile.close();
            mpFileSystem->delete_file(temp_file, false);
        } else {
            return false;
        }
    } else {
        return false;
    }
    return true;
}

bool NCBIEntrez::allowed_to_access_api() {
    std::lock_guard<std::mutex> lock(mTimeMutex);
    auto current_time = std::chrono::system_clock::now();
    uint64 time_diff = std::chrono::duration_cast<std::chrono::milliseconds>(current_time - mTimeSinceAccession).count();
    // Check if we are allowed to access the Entrez API
    if (time_diff < NCBI_ENTREZ_API_TIMEOUT) {
        // We are within the confines of the NCBI timeout, have we exceeded our accession count?
        if (mAccessionPerTimeoutCt < mAllowedAccessionsPerSecond) {
            // We have not exceeded our count yet
            mAccessionPerTimeoutCt++;
        } else {
            // We have exceeded our limit, hold up thread until we are allowed to start again
            uint64 waiting_time = NCBI_ENTREZ_API_TIMEOUT - time_diff;
            if (waiting_time > NCBI_ENTREZ_API_TIMEOUT) waiting_time = NCBI_ENTREZ_API_TIMEOUT;
            std::this_thread::sleep_for(std::chrono::milliseconds(waiting_time+1));
            mAccessionPerTimeoutCt = 1;
            current_time = std::chrono::system_clock::now();
            mTimeSinceAccession = current_time;
        }
    } else {
        // We are outside of API timeout period, start a new one
        mAccessionPerTimeoutCt = 1;
        mTimeSinceAccession = current_time;
    }
    return true;
}

std::string NCBIEntrez::generate_query(EntrezInput &entrezInput, ENTREZ_TYPES type) {

    std::string final_url;

    switch (type) {

        case ENTREZ_SEARCH:
            final_url = ESEARCH_BASE_URL;
            break;

        case ENTREZ_SUMMARY:
            final_url = ESUMMARY_BASE_URL;
            break;

        case ENTREZ_FETCH:
            final_url = EFETCH_BASE_URL;
            break;
    }

    if (!entrezInput.term.empty()) {
        process_term(entrezInput.term); // NO spaces in term
        final_url += "&" + NCBI_ENTREZ_TERM + entrezInput.term;
    }

    if (!entrezInput.database.empty()) {
        final_url += "&" + NCBI_ENTREZ_DATABASE + entrezInput.database;
    }

    if (!entrezInput.retmode.empty()) {
        final_url += "&" + NCBI_ENTREZ_RETMODE + entrezInput.retmode;
    }

    if (!entrezInput.rettype.empty()) {
        final_url += "&" + NCBI_ENTREZ_RETTYPE + entrezInput.rettype;
    }

    if (!entrezInput.uid_list.empty()) {
        final_url += "&" + NCBI_ENTREZ_UID;

        for (auto &uid : entrezInput.uid_list) {
            if (!uid.empty()) {
                final_url += uid + ",";
            }
        }
        final_url.pop_back(); // remove trailing ','
    }

    if (!mUserAPIKey.empty()) {
        final_url += "&" + NCBI_ENTREZ_API_KEY + mUserAPIKey;
    }

    // Add email
    final_url += "&" + NCBI_ENTREZ_EMAIL_KEY + ENTREZ_EMAIL_IDENTIFIER;

    // Add tool
    final_url += "&" + NCBI_ENTREZ_TOOL_KEY + ENTREZ_TOOL_IDENTIFIER;

    return final_url;
}

void NCBIEntrez::process_term(std::string &term) {
    STR_REPLACE(term, ' ', '_');
}

/*
 * GP Flat Files from NCBI generally start with the following format:
 *
 * (+)
 * DEFINITION  carbonic anhydrase 2 [Cimex lectularius].
 * ACCESSION   XP_014245616
 * VERSION     XP_014245616.1
 * DBLINK      BioProject: PRJNA298750
 * DBSOURCE    REFSEQ: accession XM_014390130.2
 * KEYWORDS    RefSeq.
 * SOURCE      Cimex lectularius (bed bug)
 * ........................ (a bit further down in file)
 *      CDS             1..298
 *                    /gene="LOC106664428"
 *                    /coded_by="XM_014390130.2:154..1050"
 *                    /db_xref="GeneID:106664428"
 *
 *  Unfortunately access to the database requires manual parsing to get specific data
 */
bool NCBIEntrez::parse_ncbi_gp_file(EntrezInput& entrezInput, EntrezResults& entrezResults, std::string &output_file) {
    std::string line;
    std::smatch match;
    std::string current_sequence;
    NCBIData entrez_entry_data;
    std::ifstream infile(output_file);
    const std::string TEST_REGEX = R"(LOCUS\s*(\S+)\s)";
    const std::string TEXT_REGEX_GENE = "\\s+\\/db_xref=\"GeneID:(.+)\"";

    // NOTE with NCBI data versions, depending on what database version we are searching against,
    //  it could be inconsistent with the latest version on NCBI. These versions are denoted after the '.'
    //  example: XP_014245616.1 is verison 1. Theoretically it could be XP_014245616.2 at some point. We
    //  also want to preserve the version that the user searched against.
    // Because of this, we are going to map  the Users data to what we find in NCBI, extremely inefficient...
    std::unordered_map<std::string, std::string> ncbi_id_mappings;
    for (std::string& entry : entrezInput.uid_list) {
        if (entry.empty()) continue;
        std::string reformatted = entry.substr(0,entry.find('.'));
        auto it = ncbi_id_mappings.find(reformatted);
        if (it == ncbi_id_mappings.end()) {
            ncbi_id_mappings.emplace(reformatted, entry);
        }
    }

    while (std::getline(infile, line)) {
        if (line.empty()) continue;

        // Find what sequence we are on (it is possible to pull data for multiple at once
        if (current_sequence.empty()) {
            if (std::regex_search(line, match, std::regex(TEST_REGEX))) {
                current_sequence = std::string(match[1]);
                current_sequence = current_sequence.substr(0, current_sequence.find('.'));
                entrez_entry_data = {};
            }
        } else {
            // Check if this line contains any data we want
            for (ENTREZ_DATA_TYPES data_type : entrezInput.data_types) {
                switch (data_type) {
                    case ENTREZ_DATA_GENEID:
                        if (std::regex_search(line, match, std::regex(TEXT_REGEX_GENE))) {
                            entrez_entry_data.geneid = std::string(match[1]);
                        }
                        break;
                    default:
                        break;

                }
            }
            // Check if have finished with this sequences
            //  NCBI denotes this as '//'
            if (line == "//") {
                auto it = ncbi_id_mappings.find(current_sequence);
                if (it != ncbi_id_mappings.end()) {
                    entrezResults.entrez_results.emplace(it->second, entrez_entry_data);
                    current_sequence = "";
                } else {
                    current_sequence = "";
                }
            }
        }
    }
    infile.close();
    return true;
}
