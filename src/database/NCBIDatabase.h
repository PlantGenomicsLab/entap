/*
* Developed by Alexander Hart
 * Plant Computational Genomics Lab
 * University of Connecticut
 *
 * For information, contact Alexander Hart at:
 *     entap.dev@gmail.com
 *
 * Copyright 2017-2025, Alexander Hart, Dr. Jill Wegrzyn
 *
 * This file is part of EnTAP.
 *
 * EnTAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnTAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnTAP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NCBIDATABASE_H
#define NCBIDATABASE_H

#include "../FileSystem.h"
#include "../common.h"
class NCBIEntrez;

struct NCBIData {
 std::string accession_id; // NCBI sequence accession ID
 std::string geneid;       // GeneID found for this sequence
};
typedef std::unordered_map<std::string, NCBIData> NCBIDataResults_t;
class NCBIDatabase {
public:
 NCBIDatabase(const NCBIDatabase&) = delete;
 NCBIDatabase& operator=(const NCBIDatabase&) = delete;
 NCBIDatabase(NCBIDatabase&&) = delete;
 NCBIDatabase& operator=(NCBIDatabase&&) = delete;
 ~NCBIDatabase();
 explicit NCBIDatabase(FileSystem *pfile_system, size_t thread_num, std::string user_api_key);
 void add_ncbi_query(const std::string &ncbi_subject_id);
 static bool is_ncbi_match(const std::string &ncbi_sseqid);
 NCBIData get_ncbi_data(const std::string &ncbi_subject_id) const;
 void wait_for_completion();

 static const std::string NCBI_UNKNOWN_GENE_ID;

private:
typedef enum {
  NCBI_ACCESSION_ENTREZ,
  NCBI_ACCESSION_API
} NCBI_ACCESSION_TYPE;

std::vector<std::thread> mThreadPool;
 bool mContinueProcessing;
 bool mProcessRemaining;
 std::queue<std::string> mProcessingQueue;
 std::unordered_map<std::string, NCBIData> mNCBIResults;
 NCBIEntrez *mpNCBIEntrez;
 NCBI_ACCESSION_TYPE mNCBIAccessionType;
 size_t mAvailableThreadNum;
 std::mutex mDataMutex;                     // Mutex for data map
 std::mutex mQueueMutex;                    // Mutex for queue access
 std::string mUserAPIKey;

 void main_loop();
 void process_queue();
 uint16 NCBI_DATABASE_RETRIES = 3; // Number of retries if a data pull fails
 uint16 ENTREZ_QUEUE_SIZE = 200;   // Bulk processing size for entrez data
};



#endif //NCBIDATABASE_H
