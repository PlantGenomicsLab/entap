/*
 *
 * Developed by Alexander Hart
 * Plant Computational Genomics Lab
 * University of Connecticut
 *
 * For information, contact Alexander Hart at:
 *     entap.dev@gmail.com
 *
 * Copyright 2017-2025, Alexander Hart, Dr. Jill Wegrzyn
 *
 * This file is part of EnTAP.
 *
 * EnTAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnTAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnTAP.  If not, see <http://www.gnu.org/licenses/>.
*/


//*********************** Includes *****************************
#include "EntapConfig.h"
#include "database/EggnogDatabase.h"
#include "TerminalCommands.h"
#include "database/BuscoDatabase.h"
#include "database/FastaDatabase.h"
#include "module_interfaces/DiamondInterface.h"
//**************************************************************

namespace entapConfig {

    // ***************** Local Variables ***************************
    EntapDatabase           *pEntapDatabase;  // Pointer to EnTAP database (binary or sql)
    std::string              binDir;          // Absolute path to binary directory
    std::string              dataDir;         // Absolute path to database directory
    std::string              rootDir;         // Absolute path to EnTAP outfiles directory
    FileSystem              *pFileSystem;     // Pointer to EnTAP filesystem
    UserInput               *pUserInput;      // Pointer to User Data
    // *************************************************************

    // ***************** Local Prototype Functions******************
    void init_entap_database();
    void init_split_fasta_databases(std::vector<FastaDatabase> &fasta_databases, const ent_input_multi_str_t &user_species, const std::string &outdir,
        const EntapDatabase* entap_database);
    void init_diamond_index(std::string &diamond_exe, uint16 threads, std::vector<FastaDatabase> &fasta_databases);
    void init_eggnog(uint16 threads, std::string &diamond_exe);
    void init_busco();
    // *************************************************************


    /**
     * ======================================================================
     * Function void init_entap(UserInput *input, FileSystem *filesystem)
     *
     * Description          - Entry into configurating EnTAP
     *                      - Responsible for downloading EnTAP databases (taxonomic,
     *                        Gene Ontology, UniProt), DIAMOND configuring, and EggNOG download
     *
     * Notes                - Entry
     *
     * @param input         - Pointer to flags from user
     * @param filesystem    - Pointer to EnTAP filesystem
     *
     * @return              - None
     *
     * =====================================================================
     */
    void execute_main(UserInput *input, FileSystem *filesystem) {
        FS_dprint("Entering EnTAP Config");
        TC_print(TC_PRINT_COUT, get_cur_time() + " -- Running EnTAP Configuration...");

        pUserInput = input;
        pFileSystem = filesystem;

        // Get directory to output databases to. Same as "outfiles" path
        rootDir = filesystem->get_root_path();

        binDir  = PATHS(rootDir, input->getBIN_PATH_DEFAULT());
        dataDir = PATHS(rootDir, input->getDATABASE_DIR_DEFAULT());

        // Create paths
        FileSystem::create_dir(rootDir);    // Should have already been created, confirm
        FileSystem::create_dir(binDir);
        FileSystem::create_dir(dataDir);

        uint16 threads = (uint16)pUserInput->get_supported_threads();
        ent_input_str_t diamond_exe = pUserInput->get_user_input<ent_input_str_t>(INPUT_FLAG_DIAMOND_EXE);
        std::vector<FastaDatabase> fasta_databases = pUserInput->get_user_fasta_databases();
        try {

            init_entap_database();

            if (pUserInput->has_input(INPUT_FLAG_DATABASE_SPLIT_TAX)) {
                ent_input_multi_str_t user_species = pUserInput->get_split_db_taxons();
                init_split_fasta_databases(fasta_databases, user_species, dataDir, pEntapDatabase);
            }

            init_diamond_index(diamond_exe, threads, fasta_databases);

            init_eggnog(threads, diamond_exe);

            // Disable BUSCO config for now
//            init_busco();

        } catch (ExceptionHandler &e){
            SAFE_DELETE(pEntapDatabase);
            throw ExceptionHandler(e.what(), e.getErr_code());
        }
        SAFE_DELETE(pEntapDatabase);
        FS_dprint("Configuration complete!");
        std::string log_msg = "\n **** Be sure to update the EnTAP ini files with the new database paths ****\n";
        pFileSystem->print_stats(log_msg);
        TC_print(TC_PRINT_COUT, get_cur_time() + " -- EnTAP Configuration Complete");
        TC_print(TC_PRINT_COUT, "\n **** Be sure to update the EnTAP ini files with the new database paths ****\n");
    }

    // WARNING will add additional new databases to fasta_databases
    void init_split_fasta_databases(std::vector<FastaDatabase> &fasta_databases, const ent_input_multi_str_t &user_species,
        const std::string &outdir, const EntapDatabase* entap_database) {
        FS_dprint("Splitting FASTA databases into user input taxons...");

        if (fasta_databases.empty() || user_species.empty() || entap_database == nullptr) return;

        std::vector<FastaDatabase> added_fasta_databases;
        std::stringstream log_msg;      // Message to print to EnTAP log file

        auto startTime = std::chrono::system_clock::now();
        TC_print(TC_PRINT_COUT, get_time_str(startTime) + " -- Preparing to split FASTA databases...");
        pFileSystem->format_stat_stream(log_msg, "FASTA Database Taxon Split");

        for (const FastaDatabase &database : fasta_databases) {
            std::string database_path = database.get_mDatabaseFilePath();
            if (!FileSystem::file_exists(database_path) ||
                FileSystem::file_empty(database_path)) {
                FS_dprint("Database does not exist or empty at: " + database_path);
                log_msg << "FASTA database does not exist or empty at: " << database_path << std::endl;
            }

            std::ifstream in_file;
            std::ofstream out_file;
            std::string line;       // Current line read from input fasta database
            std::string seq_title;  // Title of fasta sequence (containing all info of line starting with '>')
            std::string sequence;   // fasta sequence corresponding to seq title
            std::string database_filename;  // Filename of database without extension
            std::map<std::string, std::ofstream*>   file_map_taxons;

            database_filename = pFileSystem->get_filename(database_path, false);

            try {
                in_file.open(database.get_mDatabaseFilePath());
                // Create output streams for user taxons
                for (const std::string &taxon : user_species) {
                    if (taxon.empty()) continue;
                    std::string taxon_formatted = taxon;
                    STR_REPLACE(taxon_formatted, ' ', '_');
                    std::string out_filename = database_filename + "_" + taxon_formatted + FileSystem::EXT_FASTA;
                    std::string out_filepath = PATHS(outdir, out_filename);
                    if (!FileSystem::file_exists(out_filepath)) {
                        FS_dprint("Creating out file stream at: " + out_filepath);
                        TC_print(TC_PRINT_COUT, "\tFASTA Database Created At: " + out_filepath);
                        log_msg << "FASTA database created at: " << out_filepath << std::endl;
                        file_map_taxons[taxon] = new std::ofstream(out_filepath, std::ios::out | std::ios::app);
                        added_fasta_databases.emplace_back(FastaDatabase::FASTA_DATABASE_FILE_TEXT,
                                                            FastaDatabase::FASTA_DATABASE_UNKNOWN,
                                                            out_filepath, pFileSystem);
                    } else {
                        TC_print(TC_PRINT_COUT, "\tFASTA Database Already Exists At: " + out_filepath);
                        log_msg << "FASTA database already exists at: " << out_filepath << std::endl;
                    }
                }
            } catch(std::exception &e) {
                for (auto& pair : file_map_taxons) {
                    if (pair.second !=nullptr) {
                        pair.second->close();
                        delete pair.second;
                    }
                }
                throw ExceptionHandler("ERROR opening FASTA database at: " + database.get_mDatabaseFilePath() + "Error:" +
                    std::string(e.what()), ERR_ENTAP_INPUT_PARSE);
            }

            // if no output files created, skip this database
            if (file_map_taxons.empty()) continue;

            // Open database and parse
            while (true) {
                std::getline(in_file, line);
                if (line.empty() && !in_file.eof()) continue;
                if (line.find(FileSystem::FASTA_FLAG) == 0 || in_file.eof()) {
                    if (!seq_title.empty()) {
                        if (in_file.eof()) {
                            sequence += line + "\n";
                        }
                        // Have entire sequence data, check if it matches any user species
                        std::string taxon = FastaDatabase::get_species(seq_title);
                        if (!taxon.empty()) {
                            // Check if database taxon matches user taxons
                            for (const ent_input_str_t &user_taxon : user_species) {
                                if (user_species.empty()) continue;
                                if (entap_database->is_taxon_match(taxon, user_taxon)) {
                                    if (file_map_taxons.find(user_taxon) != file_map_taxons.end()) {
                                        *file_map_taxons[user_taxon] << sequence;   // already has new line
                                    }
                                }
                            }
                        }
                    }
                    if (in_file.eof()) break;
                    sequence = line + "\n";
                    seq_title = line;
                } else {
                    sequence += line + "\n";
                }
            } // END WHILE
            // Close files and add non-empty files to our final fasta database list
            in_file.close();
            for (auto& pair : file_map_taxons) {
                if (pair.second !=nullptr) {
                    pair.second->close();
                    delete pair.second;
                }
            }
        } // END FOR

        // Add files back to fasta databases so that they will be indexed by DIAMOND
        fasta_databases.insert( fasta_databases.end(), added_fasta_databases.begin(),
            added_fasta_databases.end() );

        std::string temp = log_msg.str();
        pFileSystem->print_stats(temp);

        auto endTime = std::chrono::system_clock::now();
        int64 time_diff = std::chrono::duration_cast<std::chrono::minutes>(endTime - startTime).count();
        TC_print(TC_PRINT_COUT, get_cur_time() + " -- FASTA Database Split Complete [" +
            std::to_string(time_diff) + " min]");
    }

    /**
     * ======================================================================
     * Function init_diamond_index(std::string diamond_exe,std::string out_path,
     *                             uint16 threads)
     *
     * Description          - Responsible for indexing user specified FASTA formatted
     *                        database for DIAMOND usage
     *
     * Notes                - Utilizes DIAMIND executable
     *
     * @param diamond_exe   - Path to DIAMOND exe
     * @param threads       - Thread number
     * @param fasta_databases
     *
     * @return              - None
     *
     * =====================================================================
     */
    void init_diamond_index(std::string &diamond_exe, uint16 threads, std::vector<FastaDatabase> &fasta_databases) {
        FS_dprint("Preparing to index database(s) with Diamond...");

        std::string indexed_path;       // Absolute path to final, DMND indexed database
        std::string std_out;            // Standard output (out, err) from execution
        std::string index_command;      // DMND indexing command
        std::string out_err;
        std::stringstream log_msg;      // Message to print to EnTAP log file

        if (fasta_databases.empty()) {
            FS_dprint("No databases selected, skipping");
            // Exit routine if no databases need to be configured!!
            return;
        }

        auto startTime = std::chrono::system_clock::now();
        TC_print(TC_PRINT_COUT, get_time_str(startTime) + " -- Preparing to index DIAMOND databases...");

        pFileSystem->format_stat_stream(log_msg, "DIAMOND Database Configuration");

        for (FastaDatabase &fasta_database: fasta_databases) {
            switch (fasta_database.get_mDatabaseFileFormat()) {
            case FastaDatabase::FASTA_DATABASE_FILE_DIAMOND:
                // DIAMOND database already exists, skip
                FS_dprint("File found at " + indexed_path + ".dmnd, skipping...");
                log_msg << "DIAMOND database skipped, exists at: " << indexed_path << std::endl;
                break;

            case FastaDatabase::FASTA_DATABASE_FILE_USER_CONFIG: {
                indexed_path = PATHS(binDir,fasta_database.get_database_user_string());
                std::string fasta_path = PATHS(pFileSystem->get_temp_outdir(), fasta_database.get_database_user_string() + FileSystem::EXT_FAA);
                if (!FileSystem::file_exists(indexed_path + FileSystem::EXT_DMND)) {
                    if (fasta_database.download_configure_database(fasta_path, out_err)) {
                        TC_print(TC_PRINT_COUT, "\tIndexing DIAMOND database at: " + fasta_path + "...");
                        auto index_start_time = std::chrono::system_clock::now();

                        // Configure database for diamond
                        if (DiamondInterface::Diamond_ConfigureDatabase(diamond_exe, fasta_path, indexed_path,
                            threads,out_err)) {
                            FS_dprint("Database successfully indexed to: " + indexed_path + FileSystem::EXT_DMND);
                            log_msg << "DIAMOND database generated to: " << indexed_path << FileSystem::EXT_DMND << std::endl;
                            auto index_end_time = std::chrono::system_clock::now();
                            int64 time_diff = std::chrono::duration_cast<std::chrono::minutes>(index_end_time - index_start_time).count();
                            TC_print(TC_PRINT_COUT, "\tComplete [" + std::to_string(time_diff) + " min]");
                            } else {
                                throw ExceptionHandler("Error indexing database at: " + fasta_path + "\nDIAMOND Error: " + out_err,
                                    ERR_ENTAP_INIT_INDX_DATABASE);
                            }
                    } else {
                        throw ExceptionHandler("Error downloading database to: " + fasta_path + "\n" + out_err,
                            ERR_ENTAP_INIT_INDX_DATABASE);
                    }
                } else {
                    FS_dprint("File found at " + indexed_path + ".dmnd, skipping...");
                    log_msg << "DIAMOND database skipped, exists at: " << indexed_path << std::endl;
                    TC_print(TC_PRINT_COUT, "\tDIAMOND database already exists at: " + indexed_path);
                }
                break;
            }

            case FastaDatabase::FASTA_DATABASE_FILE_TEXT: {
                std::string fasta_filepath = fasta_database.get_mDatabaseFilePath();
                indexed_path = PATHS(binDir,pFileSystem->get_filename(fasta_filepath, false));

                if (FileSystem::file_exists(indexed_path + FileSystem::EXT_DMND)) {

                    FS_dprint("File found at " + indexed_path + ".dmnd, skipping...");
                    log_msg << "DIAMOND database skipped, exists at: " << indexed_path << std::endl;
                    TC_print(TC_PRINT_COUT, "\tDIAMOND database already exists at: " + indexed_path);

                } else {
                    // NO, database does not exist

                    TC_print(TC_PRINT_COUT, "\tIndexing DIAMOND database at: " + fasta_filepath + "...");
                    auto index_start_time = std::chrono::system_clock::now();

                    // Configure database for diamond
                    if (DiamondInterface::Diamond_ConfigureDatabase(diamond_exe, fasta_filepath, indexed_path,
                        threads,out_err)) {
                        FS_dprint("Database successfully indexed to: " + indexed_path + FileSystem::EXT_DMND);
                        log_msg << "DIAMOND database generated to: " << indexed_path << FileSystem::EXT_DMND << std::endl;
                        auto index_end_time = std::chrono::system_clock::now();
                        int64 time_diff = std::chrono::duration_cast<std::chrono::minutes>(index_end_time - index_start_time).count();
                        TC_print(TC_PRINT_COUT, "\tComplete [" + std::to_string(time_diff) + " min]");
                        } else {
                            throw ExceptionHandler("Error indexing database at: " + fasta_filepath + "\nDIAMOND Error: " + out_err,
                                ERR_ENTAP_INIT_INDX_DATABASE);
                        }
                }
                break;
            }
                default:
                    break;
            }
        } // END FOR
        std::string temp = log_msg.str();
        pFileSystem->print_stats(temp);
        auto endTime = std::chrono::system_clock::now();
        int64 time_diff = std::chrono::duration_cast<std::chrono::minutes>(endTime - startTime).count();
        TC_print(TC_PRINT_COUT, get_cur_time() + " -- DIAMOND Database Indexing Complete [" +
            std::to_string(time_diff) + " min]");
    }


    /**
     * ======================================================================
     * Function init_eggnog(uint16 threads, std::string &diamond_exe)
     *
     * Description          - Downloads and configured required EggNOG databases
     *                        (SQL database and FASTA database)
     *                      - Configured FASTA database for DIAMOND if necessary
     *
     * Notes                - Utilizes DIAMOND executable
     *
     * @param threads       - Thread number for execution
     * @param diamond_exe   - Method to execute DIAMOND
     *
     * @return              - None
     *
     * =====================================================================
     */
    void init_eggnog(uint16 threads, std::string &diamond_exe) {
        std::string sql_outpath;                // Absolute path to EggNOG sql output
        std::string fasta_outpath;              // Absolute path to EggNOG fasta output
        std::string dmnd_outpath;               // Absolute path to converted FASTA -> DMND
        std::string err_msg;                    // Error mMessage from execution
        std::string index_cmd;                  // DMND indexing command
        std::string std_out;                    // Standard output (err, out) from execution
        std::stringstream log_msg;              // Message to print to EnTAP log file
        std::chrono::time_point<std::chrono::system_clock> subtask_start_time;
        std::chrono::time_point<std::chrono::system_clock> subtask_end_time;
        ent_input_multi_int_t ontology_flags;

        FS_dprint("Ensuring EggNOG databases exist...");
        auto startTime = std::chrono::system_clock::now();
        TC_print(TC_PRINT_COUT, get_time_str(startTime) + " -- Verifying EggNOG Databases...");

        pFileSystem->format_stat_stream(log_msg, "EggNOG Database Configuration");

        // Check which EggNOG app we want to use
        ontology_flags = pUserInput->get_user_input<ent_input_multi_int_t>(INPUT_FLAG_ONTOLOGY);
        // Generate database to allow downloading
        EggnogDatabase eggnogDatabase = EggnogDatabase(pFileSystem, pEntapDatabase, nullptr);
        // setup outpath
        sql_outpath   = PATHS(dataDir, pUserInput->getEGG_SQL_DB_FILENAME());
        fasta_outpath = PATHS(pFileSystem->get_temp_outdir(), "eggnog_fasta_temp.fa");
        dmnd_outpath  = PATHS(binDir, pUserInput->getEGG_DMND_FILENAME());

        for (uint16 flag : ontology_flags) {
            switch (flag) {

                case ONT_EGGNOG_MAPPER: {
                    // Check if EggNOG mapper SQL database exists
                    std::string user_egg_map_sql_db = pUserInput->get_user_input<ent_input_str_t>(INPUT_FLAG_EGG_MAPPER_DATA_DIR);
                    user_egg_map_sql_db = PATHS(user_egg_map_sql_db,pUserInput->getEGG_SQL_DB_FILENAME());
                    if (!FileSystem::file_exists(user_egg_map_sql_db) && !FileSystem::file_exists(sql_outpath)) {
                        FS_dprint("EggNOG SQL database not found at " + user_egg_map_sql_db);
                        TC_print(TC_PRINT_COUT, "\tDownloading EggNOG SQL Database to: " + sql_outpath);
                        subtask_start_time = std::chrono::system_clock::now();
                        if (eggnogDatabase.download(EggnogDatabase::EGGNOG_SQL, sql_outpath) != EggnogDatabase::ERR_EGG_OK) {
                            // Error in download
                            err_msg = "Unable to download EggNOG sql database from FTP to: " +
                                      sql_outpath + "\nError: " + eggnogDatabase.print_err();
                            throw ExceptionHandler(err_msg,ERR_ENTAP_INIT_EGGNOG);
                        } else {
                            // Downloaded successfully
                            FS_dprint("Success! EggNOG SQL database downloaded to: " + sql_outpath);
                            log_msg << "EggNOG SQL database written to: " + sql_outpath << std::endl;
                            subtask_end_time = std::chrono::system_clock::now();
                            int64 time_diff = std::chrono::duration_cast<std::chrono::minutes>(subtask_end_time - subtask_start_time).count();
                            TC_print(TC_PRINT_COUT, "\tComplete [" + std::to_string(time_diff) + " min]");
                        }

                    } else {
                        // EggNOG SQL database already exists, skip
                        std::string path;
                        if (FileSystem::file_exists(user_egg_map_sql_db)) path = user_egg_map_sql_db;
                        if (FileSystem::file_exists(sql_outpath)) path = sql_outpath;
                        FS_dprint("EggNOG SQL database already exists at: " + path + " skipping");
                        log_msg << "EggNOG SQL Database skipped, exists at: " << path << std::endl;
                        TC_print(TC_PRINT_COUT, "\tEggNOG SQL Database Already Exists At: " + path);
                    }

                    // Check if DIAMOND EggNOG database exists
                    std::string user_egg_map_dmnd = pUserInput->get_user_input<ent_input_str_t>(INPUT_FLAG_EGG_MAPPER_DMND_DB);
                    if (!FileSystem::file_exists(user_egg_map_dmnd) && !FileSystem::file_exists(dmnd_outpath)) {
                        // No, does not exist, need to dowload
                        TC_print(TC_PRINT_COUT, "\tDownloading EggNOG DIAMOND Database to: " + dmnd_outpath);
                        subtask_start_time = std::chrono::system_clock::now();
                        if (eggnogDatabase.download(EggnogDatabase::EGGNOG_DIAMOND, dmnd_outpath) != EggnogDatabase::ERR_EGG_OK) {
                            // Error in download
                            err_msg = "Unable to get EggNOG DIAMOND from FTP to: " +
                                    dmnd_outpath + "\nError: " + eggnogDatabase.print_err();
                            throw ExceptionHandler(err_msg,ERR_ENTAP_INIT_EGGNOG);
                        }

                        subtask_end_time = std::chrono::system_clock::now();
                        int64 time_diff = std::chrono::duration_cast<std::chrono::minutes>(subtask_end_time - subtask_start_time).count();
                        TC_print(TC_PRINT_COUT, "\tComplete [" + std::to_string(time_diff) + " min]");
                        FS_dprint("Success! EggNOG DIAMOND downloaded");
                        log_msg << "EggNOG DIAMOND database written to: " + dmnd_outpath << std::endl;

                    } else {
                        // Yes, file already exists
                        std::string path;
                        if (FileSystem::file_exists(user_egg_map_dmnd)) path = user_egg_map_dmnd;
                        if (FileSystem::file_exists(dmnd_outpath)) path = dmnd_outpath;
                        FS_dprint("EggNOG DIAMOND database already exists at: " + path);
                        log_msg << "EggNOG DIAMOND database skipped, exists at: " << path << std::endl;
                        TC_print(TC_PRINT_COUT, "\tEggNOG DIAMOND Database Already Exists At: " + path);
                    }
                    break;
                }

                case ONT_EGGNOG_DMND: {
                    std::string user_egg_dmnd = pUserInput->get_user_input<ent_input_str_t>(INPUT_FLAG_EGG_DMND_DB);
                    std::string user_egg_sql  = pUserInput->get_user_input<ent_input_str_t>(INPUT_FLAG_EGG_SQL_DB);

                    // Check if SQL database already exists
                    if (!pFileSystem->file_exists(user_egg_sql) && !pFileSystem->file_exists(sql_outpath)) {
                        // No, path does not. Download it
                        TC_print(TC_PRINT_COUT, "Configuring EggNOG database...");
                        if (eggnogDatabase.download(EggnogDatabase::EGGNOG_SQL, sql_outpath) != EggnogDatabase::ERR_EGG_OK) {
                            // Error in download
                            err_msg = "Unable to download EggNOG sql database from FTP to: " +
                                      sql_outpath + "\nError: " + eggnogDatabase.print_err();
                            throw ExceptionHandler(err_msg,ERR_ENTAP_INIT_EGGNOG);
                        } else {
                            // Downloaded successfully
                            FS_dprint("Success! EggNOG SQL database downloaded to: " + sql_outpath);
                            log_msg << "EggNOG SQL database written to: " + sql_outpath << std::endl;
                        }
                    } else {
                        // Already exists, skip
                        std::string path;
                        if (pFileSystem->file_exists(user_egg_sql)) path = user_egg_sql;
                        if (pFileSystem->file_exists(sql_outpath)) path = sql_outpath;
                        FS_dprint("EggNOG SQL database already exists at: " + path +
                                  " skipping");
                        log_msg << "EggNOG SQL Database skipped, exists at: " << path << std::endl;
                    }

                    // Check if DIAMOND EggNOG database exists
                    if (!pFileSystem->file_exists(user_egg_dmnd) && !pFileSystem->file_exists(dmnd_outpath)) {
                        // No, does not exist, need to generate from FASTA
                        if (eggnogDatabase.download(EggnogDatabase::EGGNOG_FASTA, fasta_outpath) != EggnogDatabase::ERR_EGG_OK) {
                            // Error in download
                            err_msg = "Unable to get EggNOG FASTA from FTP to: " +
                                      fasta_outpath + "\nError: " + eggnogDatabase.print_err();
                            throw ExceptionHandler(err_msg,ERR_ENTAP_INIT_EGGNOG);
                        }

                        // Now, index for DIAMOND
                        FS_dprint("Success! EggNOG FASTA downloaded, indexing for DIAMOND...");
                        log_msg << "EggNOG FASTA database written to: " + fasta_outpath << std::endl;

                        // TODO move to DIAMOND module
                        index_cmd =
                                diamond_exe + " makedb --in " + fasta_outpath +
                                " -d "      + dmnd_outpath +
                                " -p "      +std::to_string(threads);
                        std_out = fasta_outpath + FileSystem::EXT_STD;

                        TerminalData terminalData = TerminalData();

                        terminalData.command       = index_cmd;
                        terminalData.base_std_path = std_out;
                        terminalData.print_files   = true;
                        terminalData.suppress_std_err = false;
                        terminalData.suppress_logging = false;

                        if (TC_execute_cmd(terminalData) != 0) {
                            throw ExceptionHandler("Error indexing database at: " + dmnd_outpath + "\nError:" +
                                                   terminalData.err_stream, ERR_ENTAP_INIT_EGGNOG);
                        }
                        FS_dprint("Success! EggNOG DIAMOND database indexed at: " + dmnd_outpath);
                        log_msg << "DIAMOND EggNOG database written to: " + dmnd_outpath << std::endl;

                    } else {
                        // Yes, file already exists
                        std::string path;
                        if (pFileSystem->file_exists(user_egg_dmnd)) path = user_egg_dmnd;
                        if (pFileSystem->file_exists(dmnd_outpath)) path = dmnd_outpath;
                        FS_dprint("EggNOG DIAMOND database already exists at: " + path);
                        log_msg << "EggNOG DIAMOND database skipped, exists at: " << path << std::endl;
                        TC_print(TC_PRINT_COUT, "EggNOG database already exist, skipping...");
                    }
                    break;
                }

                default:
                    break;
            }
        }

        // Print to log/debug
        FS_dprint("Success! All EggNOG files verified");
        std::string temp = log_msg.str();
        pFileSystem->print_stats(temp);
        auto endTime = std::chrono::system_clock::now();
        int64 time_diff = std::chrono::duration_cast<std::chrono::minutes>(endTime - startTime).count();
        TC_print(TC_PRINT_COUT, get_cur_time() + " -- EggNOG Database Verification Complete [" +
            std::to_string(time_diff) + " min]");
    }

    /**
     * ======================================================================
     * Function init_entap_database()
     *
     * Description          - Downloads and configured required EggNOG databases
     *                        (SQL database and FASTA database)
     *                      - Configured FASTA database for DIAMOND if necessary
     *
     * Notes                - Requires Internet connection
     *
     *
     * @return              - None
     *
     * =====================================================================
     */
    void init_entap_database() {
        bool              generate_databases;    // Whether user would like to generate rather than download
        vect_uint16_t     databases;             // User defined types of databases to configure (SQL or Bin)
        std::string       config_outpath;        // Path to check against (from config file)
        std::string       database_outpath;      // Path to print to (also acts as default path)
        std::stringstream log_msg;               // Log mMessage to print to EnTAP log file
        EntapDatabase::DATABASE_TYPE database_type; // Type of database to configure/download (EntapDatabase.h)
        EntapDatabase::DATABASE_ERR database_err;   // Database error (EntapDatabase.h)
        std::chrono::time_point<std::chrono::system_clock> subtask_start_time;
        std::chrono::time_point<std::chrono::system_clock> subtask_end_time;

        FS_dprint("Checking EnTAP database...");
        auto startTime = std::chrono::system_clock::now();
        TC_print(TC_PRINT_COUT, get_time_str(startTime) + " -- Verifying EnTAP Databases...");

        pFileSystem->format_stat_stream(log_msg, "EnTAP Database Configuration");

        pEntapDatabase = new EntapDatabase(pFileSystem);
        if (pEntapDatabase == nullptr) {
            throw ExceptionHandler("Unable to allocate Entap Database memory", ERR_ENTAP_MEM_ALLOC);
        }

        // If user would like to generate databases rather than download them from ftp(default)
        generate_databases = pUserInput->has_input(INPUT_FLAG_DATABASE_GENERATE);

        // Check which databases they want (will always have this input, default = 0)
        databases = pUserInput->get_user_input<ent_input_multi_int_t>(INPUT_FLAG_DATABASE_TYPE);

        // Download or generate databases
        FS_dprint("Beginning to download/generate databases...");
        for (uint16 data : databases) {
            // Check database
            database_type = static_cast<EntapDatabase::DATABASE_TYPE>(data);

            // Set outpaths and paths to check against
            switch (database_type) {
                case EntapDatabase::ENTAP_SERIALIZED:
                    FS_dprint("Generating/downloading Serialized database...");
                    config_outpath  = pUserInput->get_user_input<ent_input_str_t>(INPUT_FLAG_ENTAP_DB_BIN);
                    database_outpath = PATHS(rootDir, pUserInput->getENTAP_DATABASE_BIN_DEFAULT());
                    break;

                case EntapDatabase::ENTAP_SQL:
                    FS_dprint("Generating/downloading SQL database");
                    config_outpath   = pUserInput->get_user_input<ent_input_str_t>(INPUT_FLAG_ENTAP_DB_SQL);;
                    database_outpath = PATHS(rootDir, pUserInput->getENTAP_DATABASE_SQL_DEFAULT());
                    break;

                default:
                    FS_dprint("WARNING Unrecognized database code: " + std::to_string(data));
                    continue;
            }

            // First check if this database already exists in the config outpath or default outpath
            if (FileSystem::file_exists(config_outpath) || FileSystem::file_exists(database_outpath)) {
                std::string path;
                if (FileSystem::file_exists(config_outpath)) path = config_outpath;
                if (FileSystem::file_exists(database_outpath)) path = database_outpath;
                FS_dprint("File already exists at: " + path);
                log_msg << "Database skipped, already exists at: " << path << std::endl;
                TC_print(TC_PRINT_COUT, "\tEnTAP Database Already Exists At: " + path);
                pEntapDatabase->set_database(database_type, path);
                continue; // Don't redownload
            }

            // Need to generate/download file!
            TC_print(TC_PRINT_COUT, "Configuring EnTAP database...");
            subtask_start_time = std::chrono::system_clock::now();
            if (generate_databases) {
                FS_dprint("EntapConfig: Generating database to: " + database_outpath + "...");
                database_err = pEntapDatabase->generate_database(database_type, database_outpath);
                TC_print(TC_PRINT_COUT, "\tGenerating EnTAP Database To: " + database_outpath);
            } else {
                FS_dprint("EntapConfig: Downloading database to: " + database_outpath);
                database_err = pEntapDatabase->download_database(database_type, database_outpath);
                TC_print(TC_PRINT_COUT, "\tDownloading EnTAP Database To: " + database_outpath);
            }

            // Check if successful
            if (database_err == EntapDatabase::ERR_DATA_OK) {
                FS_dprint("Success! Database written to: " + database_outpath);
                log_msg << "Database written to: " + database_outpath << std::endl;
                subtask_end_time = std::chrono::system_clock::now();
                int64 time_diff = std::chrono::duration_cast<std::chrono::minutes>(subtask_end_time - subtask_start_time).count();
                TC_print(TC_PRINT_COUT, "\tComplete [" + std::to_string(time_diff) + " min]");
            } else {
                // Fatal if any databases fail
                throw ExceptionHandler(pEntapDatabase->print_error_log(), ERR_ENTAP_INIT_DATA_GENERIC);
            }
        } // END FOR

        // print to log
        std::string temp = log_msg.str();
        pFileSystem->print_stats(temp);
        auto endTime = std::chrono::system_clock::now();
        int64 time_diff = std::chrono::duration_cast<std::chrono::minutes>(endTime - startTime).count();
        TC_print(TC_PRINT_COUT, get_cur_time() + " -- EnTAP Database Verification Complete [" +
            std::to_string(time_diff) + " min]");
    }

    /**
     * ======================================================================
     * Function init_busco()
     *
     * Description          - Downloads and configures required files for the
     *                        BUSCO module from user input (OrthoDB datasets)
     *
     * Notes                - Require Internet connection
     *
     *
     * @return              - None
     *
     * =====================================================================
     */
    void init_busco() {
        std::string busco_outpath;          // Absolute path to download BUSCO database to
        ent_input_multi_str_t busco_databases;         // BUSCO database tag/URL input by user
        std::string busco_out_filename;     // Filename of output BUSCO database
        BuscoDatabase::BUSCO_DB_ERR busco_db_err;   // BUSCO database error code
        std::stringstream log_msg;               // Log message to print to EnTAP log file

        if (!pUserInput->has_input(INPUT_FLAG_BUSCO_DATABASE)) {
            FS_dprint("No BUSCO database input by the user, skipping...");
            return; // EXIT - user has not input a BUSCO database
        }

        FS_dprint("Initializing BUSCO database...");
        busco_db_err = BuscoDatabase::ERR_DATA_OK;
        pFileSystem->format_stat_stream(log_msg, "BUSCO Database Configuration");
        busco_databases = pUserInput->get_user_input<ent_input_multi_str_t>(INPUT_FLAG_BUSCO_DATABASE);

        for (std::string &busco_database : busco_databases) {
            if (pFileSystem->is_url(busco_database)) {
                busco_out_filename = pFileSystem->get_filename(busco_database, false);
            } else {
                busco_out_filename = busco_database;
            }
            busco_outpath = PATHS(dataDir, busco_out_filename);

            if (pFileSystem->file_exists(busco_outpath)) {
                FS_dprint("BUSCO database already exists at: " + busco_outpath);
                log_msg << "BUSCO Database skipped, already exists at: " << busco_outpath << std::endl;

            } else {
                // Database does not exist, download it
                BuscoDatabase buscoDatabase = BuscoDatabase(pFileSystem);
                busco_db_err = buscoDatabase.download_database(busco_database, busco_outpath);

                if (busco_db_err == BuscoDatabase::ERR_DATA_OK) {
                    FS_dprint("Success! BUSCO database downloaded to: " + busco_outpath);
                    log_msg << "BUSCO Database written to: " << busco_outpath << std::endl;
                } else {
                    // Fatal if database download fails
                    throw ExceptionHandler(buscoDatabase.print_error_log(), ERR_ENTAP_INIT_BUSCO_GENERIC);
                }
            }
        }

        std::string temp = log_msg.str();
        pFileSystem->print_stats(temp);
    }
}
