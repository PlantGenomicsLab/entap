/*
*
 * Developed by Alexander Hart
 * Plant Computational Genomics Lab
 * University of Connecticut
 *
 * For information, contact Alexander Hart at:
 *     entap.dev@gmail.com
 *
 * Copyright 2017-2025, Alexander Hart, Dr. Jill Wegrzyn
 *
 * This file is part of EnTAP.
 *
 * EnTAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnTAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnTAP.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "DiamondInterface.h"

#include "../FileSystem.h"
#include "../TerminalCommands.h"

namespace DiamondInterface {
     bool Diamond_ConfigureDatabase(const std::string& dmnd_exe, const std::string& inpath, const std::string& outpath, uint32 threads,
                                    std::string &out_err) {
         TerminalData terminal_data = TerminalData();
         terminal_data.command = dmnd_exe + " makedb --in " + inpath + " -d " + outpath + " -p " + std::to_string(threads);
         terminal_data.base_std_path = outpath + FileSystem::EXT_STD;
         terminal_data.print_files = true;
         terminal_data.suppress_std_err = false;
         terminal_data.suppress_logging = false;

         if (TC_execute_cmd(terminal_data) != 0) {
             out_err = terminal_data.err_stream;
             return false;
         }
        return true;
     }
}
