/*
*
 * Developed by Alexander Hart
 * Plant Computational Genomics Lab
 * University of Connecticut
 *
 * For information, contact Alexander Hart at:
 *     entap.dev@gmail.com
 *
 * Copyright 2017-2025, Alexander Hart, Dr. Jill Wegrzyn
 *
 * This file is part of EnTAP.
 *
 * EnTAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnTAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnTAP.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../QueryAlignment.h"
#ifdef ENABLE_TESTING
#include "gtest/gtest.h"
#include "../QuerySequence.h"


namespace {

    TEST(QuerySequence, Query_CalcSequenceLength) {
        // Test nucleotide sequence length
        std::string sequence = "AGCTATGCTAGCTGG";
        std::string seq_id = "test_seq";
        QuerySequence query = QuerySequence(false, sequence, seq_id);
        ASSERT_EQ(15, query.get_sequence_length());
        EXPECT_TRUE(query.QUERY_FLAG_GET(QuerySequence::QUERY_IS_NUCLEOTIDE));
        EXPECT_FALSE(query.QUERY_FLAG_GET(QuerySequence::QUERY_IS_PROTEIN));
        ASSERT_EQ(query.get_sequence(), sequence);
        ASSERT_EQ(query.get_sequence_n(), sequence);
        ASSERT_EQ(query.get_sequence_p(), "");

        // Test protein sequence length
        sequence = "FJSALASDC";
        QuerySequence query2 = QuerySequence(true, sequence, seq_id);
        ASSERT_EQ(27, query2.get_sequence_length());
        EXPECT_FALSE(query2.QUERY_FLAG_GET(QuerySequence::QUERY_IS_NUCLEOTIDE));
        EXPECT_TRUE(query2.QUERY_FLAG_GET(QuerySequence::QUERY_IS_PROTEIN));
        ASSERT_EQ(query2.get_sequence(), sequence);
        ASSERT_EQ(query2.get_sequence_n(), "");
        ASSERT_EQ(query2.get_sequence_p(), sequence);
    }

    /*  SIMILARITY SEARCH BEST HIT ALGORITHM
     *  ----------------------
     *
     *  For 1 database best hit:
     *      1. If log(e-value) difference >= 8, return BETTER e-value
     *      2. If coverage difference > 5, return BETTER coverage
     *      3. If one contaminant, one not, return NON-CONTAMINANT
     *      4. Return better tax score (combination of informative + lineage)
     *
     */
    TEST(QuerySequence, BestHit_SimSearch_OneDatabase) {
        QuerySequence::SimSearchResults simSearchResults;
        std::string query_id = "query_1";
        std::string sequence = "AGCTATGCTAGCTGG";
        QuerySequence query1 = QuerySequence(true, sequence, query_id);
        std::string user_lineage = "homo sapiens";
        std::string database1 = "database/path";
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_BLASTED));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_BLAST_HIT));

        // Alignment 1
        std::string subject1 = "subject_1";
        simSearchResults.database_path = database1;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject1;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-50;
        simSearchResults.coverage_raw = 65;
        simSearchResults.contaminant = true;
        simSearchResults.is_informative = false;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database1,
            user_lineage);

        // Verify we get back the same sequence as a best hit (only 1 hit here)
        auto *alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject1);
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                    SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject1);

        EXPECT_TRUE(query1.hit_database(SIMILARITY_SEARCH, SIM_DIAMOND, database1));

        // Alignment 2 - MUCH worse e-value compared to alignment 1, similar coverage
        std::string subject2 = "subject_2";
        simSearchResults = {};
        simSearchResults.database_path = database1;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject2;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-10;
        simSearchResults.coverage_raw = 65;
        simSearchResults.contaminant = true;
        simSearchResults.is_informative = false;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database1,
            user_lineage);

        // Verify we get back alignment_1 due to better e-value
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject1);
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                    SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject1);

        // Alignment 3 - BEST e-value compared to alignment 1,2
        std::string subject3 = "subject_3";
        simSearchResults = {};
        simSearchResults.database_path = database1;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject3;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-60;
        simSearchResults.coverage_raw = 65;
        simSearchResults.contaminant = true;
        simSearchResults.is_informative = false;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database1,
            user_lineage);
        // Verify we get back alignment_3 due to better e-value
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject3);
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                    SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject3);


        // Alignment 4 - Bad e-value, FALSE contaminant
        std::string subject4 = "subject_4";
        simSearchResults = {};
        simSearchResults.database_path = database1;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject4;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-10;
        simSearchResults.coverage_raw = 65;
        simSearchResults.contaminant = false;
        simSearchResults.is_informative = false;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database1,
            user_lineage);
        // Verify we get back alignment_3 due to better e-value regardless of contmainant status
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject3);
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                    SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject3);


        // Alignment 5 - Bad e-value, TRUE informative
        std::string subject5 = "subject_5";
        simSearchResults = {};
        simSearchResults.database_path = database1;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject5;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-10;
        simSearchResults.coverage_raw = 65;
        simSearchResults.contaminant = false;
        simSearchResults.is_informative = true;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database1,
            user_lineage);
        // Verify we get back alignment_3 due to better e-value regardless of informative status
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject3);
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                    SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject3);

        // Alignment 6 - Bad e-value, GOOD coverage
        std::string subject6 = "subject_6";
        simSearchResults = {};
        simSearchResults.database_path = database1;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject6;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-10;
        simSearchResults.coverage_raw = 100;
        simSearchResults.contaminant = true;
        simSearchResults.is_informative = false;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database1,
            user_lineage);
        // Verify we get back alignment_3 due to better e-value regardless of coverage
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject3);
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                    SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject3);


        // Alignment 7 - Similar to BEST e-value, FALSE contaminant
        std::string subject7 = "subject_7";
        simSearchResults = {};
        simSearchResults.database_path = database1;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject7;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-101;
        simSearchResults.coverage_raw = 50;
        simSearchResults.contaminant = false;
        simSearchResults.is_informative = false;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database1,
            user_lineage);
        // Verify we get back alignment_7 due to similar e-value and FALSE contaminant status
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject7);
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                    SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject7);


        // Alignment 8 - Similar to BEST e-value, GOOD coverage
        std::string subject8 = "subject_8";
        simSearchResults = {};
        simSearchResults.database_path = database1;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject8;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-98;
        simSearchResults.coverage_raw = 100;
        simSearchResults.contaminant = false;
        simSearchResults.is_informative = false;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database1,
            user_lineage);
        // Verify we get back alignment_8 due to similar e-value, GOOD coverage
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject8);
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                    SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject8);


        // Alignment 9 - Similar to BEST e-value, GOOD coverage, FALSE contaminant, TRUE INFORM
        std::string subject9 = "subject_9";
        simSearchResults = {};
        simSearchResults.database_path = database1;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject9;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-106;
        simSearchResults.coverage_raw = 100;
        simSearchResults.contaminant = false;
        simSearchResults.is_informative = true;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database1,
            user_lineage);
        // Verify we get back alignment_9 due to similar e-value/coverage, TRUE inform
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject9);
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                    SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject9);


        // Alignment 10 - Similar to BEST e-value, GOOD coverage, TRUE contaminant, TRUE INFORM
        std::string subject10 = "subject_10";
        simSearchResults = {};
        simSearchResults.database_path = database1;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject10;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-106;
        simSearchResults.coverage_raw = 100;
        simSearchResults.contaminant = true;
        simSearchResults.is_informative = true;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database1,
            user_lineage);
        // Verify we get back alignment_9 due to similar e-value/coverage, TRUE inform, TRUE contam
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject9);
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                    SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject9);
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_CONTAMINANT));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_SIM_SEARCH_CONTAM));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_INFORMATIVE));

        // Alignment 11 - Similar to BEST e-value, similar coverage, TRUE contaminant, FALSE inform
        std::string subject11 = "subject_11";
        simSearchResults = {};
        simSearchResults.database_path = database1;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject11;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-150;
        simSearchResults.coverage_raw = 100;
        simSearchResults.contaminant = true;
        simSearchResults.is_informative = true;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database1,
            user_lineage);
        // Verify we get back alignment_11 due to absolute best evalue/coverage
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject11);
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                    SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject11);
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_CONTAMINANT));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_SIM_SEARCH_CONTAM));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_INFORMATIVE));
    }

    /*  SIMILARITY SEARCH BEST HIT ALGORITHM
     *  ----------------------
     *
     *  For >1 database best hit:
     *      1. If coverage difference > 5, return BETTER coverage
     *      2. If one contaminant, one not, return NON-CONTAMINANT
     *      3. Return better tax score (combination of informative + lineage)
     *
     */
    TEST(QuerySequence, BestHit_SimSearch_MultiDatabase) {
        QuerySequence::SimSearchResults simSearchResults;
        std::string query_id = "query_1";
        std::string sequence = "AGCTATGCTAGCTGG";
        QuerySequence query1 = QuerySequence(true, sequence, query_id);
        std::string user_lineage = "homo sapiens";
        std::string database1 = "database/path1";
        std::string database2 = "database/path2";

        // Alignment 1 - Database 1
        std::string subject1 = "subject_1";
        simSearchResults.database_path = database1;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject1;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-10;
        simSearchResults.coverage_raw = 80;
        simSearchResults.contaminant = false;
        simSearchResults.is_informative = false;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database1,
            user_lineage);

        // Verify we get back the same sequence as a best hit (only 1 hit here)
        auto *alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject1);
        EXPECT_TRUE(query1.hit_database(SIMILARITY_SEARCH, SIM_DIAMOND, database1));

        // Alignment 2 - Database 2 - GOOD e-value, BAD coverage
        std::string subject2 = "subject_2";
        simSearchResults = {};
        simSearchResults.database_path = database2;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject2;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-100;
        simSearchResults.coverage_raw = 10;
        simSearchResults.contaminant = false;
        simSearchResults.is_informative = false;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database2,
            user_lineage);

        // Verify we get back Alignent1 from first database
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject1);
        // Verify we get back Alignent2 from second database
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database2);
        ASSERT_EQ(alignment->get_results()->sseqid, subject2);
        // Verify OVERALL we get back Alignent1 due to ignoring E-value and taking better coverage
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject1);
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_CONTAMINANT));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_SIM_SEARCH_CONTAM));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_INFORMATIVE));

        // Alignment 3 - Database 1 - BEST e-value compared to alignment 1,2
        std::string subject3 = "subject_3";
        simSearchResults = {};
        simSearchResults.database_path = database1;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject3;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-120;
        simSearchResults.coverage_raw = 80;
        simSearchResults.contaminant = false;
        simSearchResults.is_informative = false;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database1,
            user_lineage);
        // Verify we get back alignment_3 due to better e-value from database 1
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject3);
        // Verify second database was unaffected
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database2);
        ASSERT_EQ(alignment->get_results()->sseqid, subject2);
        // Verify OVERALL we get back Alignent3 due to better overall coverage and evalue
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject3);
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_CONTAMINANT));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_SIM_SEARCH_CONTAM));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_INFORMATIVE));

        // Alignment 4 - Database 2 - Bad e-value, similar coverage
        std::string subject4 = "subject_4";
        simSearchResults = {};
        simSearchResults.database_path = database1;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject4;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-70;
        simSearchResults.coverage_raw = 10;
        simSearchResults.contaminant = false;
        simSearchResults.is_informative = false;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database1,
            user_lineage);
        // Verify database 1 was unaffected
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject3);
        // Verify second database was unaffected
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database2);
        ASSERT_EQ(alignment->get_results()->sseqid, subject2);
        // Verify overall was unaffected
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject3);
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_CONTAMINANT));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_SIM_SEARCH_CONTAM));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_INFORMATIVE));

        // Alignment 5 - Database 2 - best overall so far, TRUE contam, TRUE inform
        std::string subject5 = "subject_5";
        simSearchResults = {};
        simSearchResults.database_path = database2;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject5;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-130;
        simSearchResults.coverage_raw = 100;
        simSearchResults.contaminant = true;
        simSearchResults.is_informative = true;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database2,
            user_lineage);
        // Verify database 1 was unaffected
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject3);
        // Verify second database is now pulling subject5
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database2);
        ASSERT_EQ(alignment->get_results()->sseqid, subject5);
        // Verify overall was updated for subject5
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject5);
        // Verify flags now TRUE
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_CONTAMINANT));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_SIM_SEARCH_CONTAM));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_INFORMATIVE));

        // Alignment 6 - Database 1 - similar to subject5 (best overall upt o now), but FALSE contam
        std::string subject6 = "subject_6";
        simSearchResults = {};
        simSearchResults.database_path = database1;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject6;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-131;
        simSearchResults.coverage_raw = 97;
        simSearchResults.contaminant = false;
        simSearchResults.is_informative = false;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database1,
            user_lineage);
        // Verify database 1 best hit is now subject6
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject6);
        // Verify second database unaffected
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database2);
        ASSERT_EQ(alignment->get_results()->sseqid, subject5);
        // Verify overall was updated for subject6
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject6);
        // Verify flags updated
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_CONTAMINANT));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_SIM_SEARCH_CONTAM));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_INFORMATIVE));

        // Alignment 7 - Database 1 - worse eval/coverage to subject6
        std::string subject7 = "subject_7";
        simSearchResults = {};
        simSearchResults.database_path = database1;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject7;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-50;
        simSearchResults.coverage_raw = 50;
        simSearchResults.contaminant = false;
        simSearchResults.is_informative = false;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database1,
            user_lineage);
        // Verify database 1 unaffected
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject6);
        // Verify second database unaffected
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database2);
        ASSERT_EQ(alignment->get_results()->sseqid, subject5);
        // Verify overall unaffected
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject6);
        // Verify flags updated
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_CONTAMINANT));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_SIM_SEARCH_CONTAM));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_INFORMATIVE));

        // Alignment 8 - Database 2 - worse eval/coverage to subject6
        std::string subject8 = "subject_8";
        simSearchResults = {};
        simSearchResults.database_path = database1;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject8;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-50;
        simSearchResults.coverage_raw = 50;
        simSearchResults.contaminant = false;
        simSearchResults.is_informative = false;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database1,
            user_lineage);
        // Verify database 1 unaffected
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject6);
        // Verify second database unaffected
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database2);
        ASSERT_EQ(alignment->get_results()->sseqid, subject5);
        // Verify overall unaffected
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject6);
        // Verify flags updated
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_CONTAMINANT));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_SIM_SEARCH_CONTAM));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_INFORMATIVE));

        // Alignment 9 - Database 1 - similar to subject6 (current best), but INFORMATIVE
        std::string subject9 = "subject_9";
        simSearchResults = {};
        simSearchResults.database_path = database1;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject9;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-134;
        simSearchResults.coverage_raw = 100;
        simSearchResults.contaminant = false;
        simSearchResults.is_informative = true;
        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database1,
            user_lineage);
        // Verify database 1 best hit is now subject9
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject9);
        // Verify second database unaffected
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database2);
        ASSERT_EQ(alignment->get_results()->sseqid, subject5);
        // Verify overall was updated for subject9
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject9);
        // Verify flags updated
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_CONTAMINANT));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_SIM_SEARCH_CONTAM));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_INFORMATIVE));

        // Alignment 10 - Database 1 - better eval, lower coverage than subject 9
        std::string subject10 = "subject_10";
        simSearchResults = {};
        simSearchResults.database_path = database1;
        simSearchResults.qseqid = query_id;
        simSearchResults.sseqid = subject10;
        simSearchResults.lineage = "homo sapiens";
        simSearchResults.species = "homo sapiens";
        simSearchResults.e_val_raw = 1E-200;
        simSearchResults.coverage_raw = 80;
        simSearchResults.contaminant = true;
        simSearchResults.is_informative = true;

        query1.add_alignment(SIMILARITY_SEARCH, SIM_DIAMOND,simSearchResults,database1,
            user_lineage);
        // Verify database 1 best hit is now subject10
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject10);
        // Verify second database unaffected
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,database2);
        ASSERT_EQ(alignment->get_results()->sseqid, subject5);
        // Verify overall was updated for subject10
        alignment = query1.get_best_hit_alignment<SimSearchAlignment>(
                            SIMILARITY_SEARCH, SIM_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject5); // verify requirement here for
                                                                // high eval/low coverage of same database
                                                                // for overall
        // Verify flags updated
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_CONTAMINANT));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_SIM_SEARCH_CONTAM));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_INFORMATIVE));


    }

    /*  EGGNOG MAPPER BEST HIT ALGORITHM
     *  ----------------------
     *
     *  1. Take best e-value (lowest)
     *
     */
    TEST(QuerySequence, BestHit_EggnogMapper) {
        QuerySequence::EggnogResults eggnog_results;
        std::string query_id = "query_1";
        std::string sequence = "AGCTATGCTAGCTGG";
        QuerySequence query1 = QuerySequence(true, sequence, query_id);
        std::string database1 = "database/path";

        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_EGGNOG_HIT));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_FAMILY_CONTAM));

        // Alignment 1
        std::string subject1 = "subject_1";
        eggnog_results.is_contaminant = true;
        eggnog_results.seed_eval_raw = 1E-10;
        eggnog_results.seed_coverage = "30";
        eggnog_results.name = subject1;

        query1.add_alignment(GENE_ONTOLOGY, ONT_EGGNOG_MAPPER,eggnog_results,database1);

        // Verify we get back the same sequence as a best hit (only 1 hit here)
        auto *alignment = query1.get_best_hit_alignment<EggnogDmndAlignment>(
                            GENE_ONTOLOGY, ONT_EGGNOG_MAPPER,database1);
        ASSERT_EQ(alignment->get_results()->name, subject1);
        alignment = query1.get_best_hit_alignment<EggnogDmndAlignment>(
                    GENE_ONTOLOGY, ONT_EGGNOG_MAPPER,"");
        ASSERT_EQ(alignment->get_results()->name, subject1);
        EXPECT_TRUE(query1.hit_database(GENE_ONTOLOGY, ONT_EGGNOG_MAPPER, database1));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_EGGNOG_HIT));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_FAMILY_CONTAM));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_CONTAMINANT));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_SIM_SEARCH_CONTAM));


        // Alignment 2 - better e-value
        std::string subject2 = "subject_2";
        eggnog_results.is_contaminant = true;
        eggnog_results.seed_eval_raw = 1E-50;
        eggnog_results.seed_coverage = "30";
        eggnog_results.name = subject2;

        query1.add_alignment(GENE_ONTOLOGY, ONT_EGGNOG_MAPPER,eggnog_results,database1);

        // Verify we get back the same sequence as a best hit (only 1 hit here)
        alignment = query1.get_best_hit_alignment<EggnogDmndAlignment>(
                            GENE_ONTOLOGY, ONT_EGGNOG_MAPPER,database1);
        ASSERT_EQ(alignment->get_results()->name, subject2);
        alignment = query1.get_best_hit_alignment<EggnogDmndAlignment>(
                    GENE_ONTOLOGY, ONT_EGGNOG_MAPPER,"");
        ASSERT_EQ(alignment->get_results()->name, subject2);
        EXPECT_TRUE(query1.hit_database(GENE_ONTOLOGY, ONT_EGGNOG_MAPPER, database1));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_EGGNOG_HIT));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_FAMILY_CONTAM));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_CONTAMINANT));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_SIM_SEARCH_CONTAM));

        // Alignment 3 - best coverage (should be ignored)
        std::string subject3 = "subject_3";
        eggnog_results.is_contaminant = true;
        eggnog_results.seed_eval_raw = 1E-20;
        eggnog_results.seed_coverage = "100";
        eggnog_results.name = subject3;

        query1.add_alignment(GENE_ONTOLOGY, ONT_EGGNOG_MAPPER,eggnog_results,database1);

        // Verify we get back the same sequence as a best hit (only 1 hit here)
        alignment = query1.get_best_hit_alignment<EggnogDmndAlignment>(
                            GENE_ONTOLOGY, ONT_EGGNOG_MAPPER,database1);
        ASSERT_EQ(alignment->get_results()->name, subject2);
        alignment = query1.get_best_hit_alignment<EggnogDmndAlignment>(
                    GENE_ONTOLOGY, ONT_EGGNOG_MAPPER,"");
        ASSERT_EQ(alignment->get_results()->name, subject2);
        EXPECT_TRUE(query1.hit_database(GENE_ONTOLOGY, ONT_EGGNOG_MAPPER, database1));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_EGGNOG_HIT));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_FAMILY_CONTAM));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_CONTAMINANT));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_SIM_SEARCH_CONTAM));

        // Alignment 4 - best evalue, non contam
        std::string subject4 = "subject_4";
        eggnog_results.is_contaminant = false;
        eggnog_results.seed_eval_raw = 1E-100;
        eggnog_results.seed_coverage = "100";
        eggnog_results.name = subject4;

        query1.add_alignment(GENE_ONTOLOGY, ONT_EGGNOG_MAPPER,eggnog_results,database1);

        // Verify we get back the same sequence as a best hit (only 1 hit here)
        alignment = query1.get_best_hit_alignment<EggnogDmndAlignment>(
                            GENE_ONTOLOGY, ONT_EGGNOG_MAPPER,database1);
        ASSERT_EQ(alignment->get_results()->name, subject4);
        alignment = query1.get_best_hit_alignment<EggnogDmndAlignment>(
                    GENE_ONTOLOGY, ONT_EGGNOG_MAPPER,"");
        ASSERT_EQ(alignment->get_results()->name, subject4);
        EXPECT_TRUE(query1.hit_database(GENE_ONTOLOGY, ONT_EGGNOG_MAPPER, database1));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_EGGNOG_HIT));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_FAMILY_CONTAM));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_CONTAMINANT));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_SIM_SEARCH_CONTAM));

        // Alignment 5 - best evalue, contam (should ignore contam status)
        std::string subject5 = "subject_5";
        eggnog_results.is_contaminant = true;
        eggnog_results.seed_eval_raw = 1E-101;
        eggnog_results.seed_coverage = "100";
        eggnog_results.name = subject5;

        query1.add_alignment(GENE_ONTOLOGY, ONT_EGGNOG_MAPPER,eggnog_results,database1);

        // Verify we get back the same sequence as a best hit (only 1 hit here)
        alignment = query1.get_best_hit_alignment<EggnogDmndAlignment>(
                            GENE_ONTOLOGY, ONT_EGGNOG_MAPPER,database1);
        ASSERT_EQ(alignment->get_results()->name, subject5);
        alignment = query1.get_best_hit_alignment<EggnogDmndAlignment>(
                    GENE_ONTOLOGY, ONT_EGGNOG_MAPPER,"");
        ASSERT_EQ(alignment->get_results()->name, subject5);
        EXPECT_TRUE(query1.hit_database(GENE_ONTOLOGY, ONT_EGGNOG_MAPPER, database1));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_EGGNOG_HIT));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_FAMILY_CONTAM));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_CONTAMINANT));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_SIM_SEARCH_CONTAM));
    }

        /*  EGGNOG MAPPER BEST HIT ALGORITHM
     *  ----------------------
     *
     *  1. Take best e-value (lowest)
     *
     */
    TEST(QuerySequence, BestHit_HorizontalGeneTransfer) {
        QuerySequence::HorizontalGeneTransferResults horizontal_gene_transfer_results;
        std::string query_id = "query_1";
        std::string sequence = "AGCTATGCTAGCTGG";
        QuerySequence query1 = QuerySequence(true, sequence, query_id);
        std::string database1 = "database/path";

        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_HGT_BLASTED));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_HGT_CANDIDATE));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_HGT_CONFIRMED));

        // Alignment 1
        std::string subject1 = "subject_1";
        horizontal_gene_transfer_results.coverage_raw = 50;
        horizontal_gene_transfer_results.e_val_raw = 1E-10;
        horizontal_gene_transfer_results.tax_score = 23;
        horizontal_gene_transfer_results.sseqid = subject1;

        query1.add_alignment(HORIZONTAL_GENE_TRANSFER, HGT_DIAMOND,horizontal_gene_transfer_results,database1);

        // Verify we get back the same sequence as a best hit (only 1 hit here)
        auto *alignment = query1.get_best_hit_alignment<HorizontalGeneTransferDmndAlignment>(
                            HORIZONTAL_GENE_TRANSFER, HGT_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject1);
        alignment = query1.get_best_hit_alignment<HorizontalGeneTransferDmndAlignment>(
                    HORIZONTAL_GENE_TRANSFER, HGT_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject1);
        EXPECT_TRUE(query1.hit_database(HORIZONTAL_GENE_TRANSFER, HGT_DIAMOND, database1));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_HGT_BLASTED));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_HGT_CANDIDATE));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_HGT_CONFIRMED));

        // Alignment 2 (better eval)
        std::string subject2 = "subject_2";
        horizontal_gene_transfer_results.coverage_raw = 50;
        horizontal_gene_transfer_results.e_val_raw = 1E-25;
        horizontal_gene_transfer_results.tax_score = 23;
        horizontal_gene_transfer_results.sseqid = subject2;

        query1.add_alignment(HORIZONTAL_GENE_TRANSFER, HGT_DIAMOND,horizontal_gene_transfer_results,database1);

        // Verify we get back the same sequence as a best hit (only 1 hit here)
        alignment = query1.get_best_hit_alignment<HorizontalGeneTransferDmndAlignment>(
                            HORIZONTAL_GENE_TRANSFER, HGT_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject2);
        alignment = query1.get_best_hit_alignment<HorizontalGeneTransferDmndAlignment>(
                    HORIZONTAL_GENE_TRANSFER, HGT_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject2);
        EXPECT_TRUE(query1.hit_database(HORIZONTAL_GENE_TRANSFER, HGT_DIAMOND, database1));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_HGT_BLASTED));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_HGT_CANDIDATE));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_HGT_CONFIRMED));

        // Alignment 3 (best coverage, should be ignored)
        std::string subject3 = "subject_3";
        horizontal_gene_transfer_results.coverage_raw = 100;
        horizontal_gene_transfer_results.e_val_raw = 1E-20;
        horizontal_gene_transfer_results.tax_score = 100;
        horizontal_gene_transfer_results.sseqid = subject3;

        query1.add_alignment(HORIZONTAL_GENE_TRANSFER, HGT_DIAMOND,horizontal_gene_transfer_results,database1);

        // Verify we get back the same sequence as a best hit (only 1 hit here)
        alignment = query1.get_best_hit_alignment<HorizontalGeneTransferDmndAlignment>(
                            HORIZONTAL_GENE_TRANSFER, HGT_DIAMOND,database1);
        ASSERT_EQ(alignment->get_results()->sseqid, subject2);
        alignment = query1.get_best_hit_alignment<HorizontalGeneTransferDmndAlignment>(
                    HORIZONTAL_GENE_TRANSFER, HGT_DIAMOND,"");
        ASSERT_EQ(alignment->get_results()->sseqid, subject2);
        EXPECT_TRUE(query1.hit_database(HORIZONTAL_GENE_TRANSFER, HGT_DIAMOND, database1));
        EXPECT_TRUE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_HGT_BLASTED));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_HGT_CANDIDATE));
        EXPECT_FALSE(query1.QUERY_FLAG_GET(QuerySequence::QUERY_HGT_CONFIRMED));
    }


}

#endif