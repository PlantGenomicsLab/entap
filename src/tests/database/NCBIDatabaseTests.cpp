/*
* Developed by Alexander Hart
 * Plant Computational Genomics Lab
 * University of Connecticut
 *
 * For information, contact Alexander Hart at:
 *     entap.dev@gmail.com
 *
 * Copyright 2017-2025, Alexander Hart, Dr. Jill Wegrzyn
 *
 * This file is part of EnTAP.
 *
 * EnTAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnTAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnTAP.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef ENABLE_TESTING
#include <random>
#include <__random/random_device.h>
#include "gtest/gtest.h"
#include "../../FileSystem.h"
#include "../../database/NCBIDatabase.h"

namespace {

     std::string generateRandomString(size_t length) {
        const std::string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        std::random_device rd;
        std::mt19937 generator(rd());
        std::uniform_int_distribution<> distribution(0, characters.size() - 1);
        std::string random_string;
        for (size_t i = 0; i < length; ++i) {
            random_string += characters[distribution(generator)];
        }
        return random_string;
    }

    TEST(NCBIDatabase, ProteinDatabase) {
        auto const startTime = std::chrono::system_clock::now();
        auto *pfilesystem = new FileSystem(startTime);
        NCBIDataResults_t ncbi_data_results;

        // Test 1, pulling gene id from protein database for a single NCBI ID
        {
            std::string test_ncbi_id = "XP_014245616";
            std::string test_expected_geneid = "106664428";
            NCBIDatabase ncbi_database(pfilesystem, 1,"");
            ncbi_database.add_ncbi_query(test_ncbi_id);
            ncbi_database.wait_for_completion();

            ASSERT_EQ(ncbi_database.get_ncbi_data(test_ncbi_id).geneid, test_expected_geneid);
        }

        // Test 2, pulling gene id from protein database for multiple NCBI ID
        //  including NCBI versions (XXXX.1)
        {
            NCBIDatabase ncbi_database(pfilesystem, 3,"");
            ncbi_database.add_ncbi_query("XP_014245616");
            ncbi_database.add_ncbi_query("XP_020482136.1");
            ncbi_database.add_ncbi_query("XP_026476231.1");
            ncbi_database.wait_for_completion();

            EXPECT_EQ(ncbi_database.get_ncbi_data("XP_014245616").geneid, "106664428");
            EXPECT_EQ(ncbi_database.get_ncbi_data("XP_020482136.1").geneid, "109976342");
            EXPECT_EQ(ncbi_database.get_ncbi_data("XP_026476231.1").geneid, "113381705");
        }

        // Test, NCBIDatabase returns empty string if cannot find ID
        {
            NCBIDatabase ncbi_database(pfilesystem, 1,"");
            ncbi_database.add_ncbi_query("XP_014dfasdfa616");
            ncbi_database.wait_for_completion();

            EXPECT_TRUE(ncbi_database.get_ncbi_data("XP_014dfasdfa616").geneid.empty());
        }

         // Test 2, one good accession with multiple random. multithreaded
         {
            std::string test_ncbi_id = "XP_014245616";
            std::string test_expected_geneid = "106664428";
            NCBIDatabase ncbi_database(pfilesystem, 5,"");
            ncbi_database.add_ncbi_query(test_ncbi_id);

            for (int i = 0; i < 30; i++) {
                // Add fake data
                ncbi_database.add_ncbi_query(generateRandomString(i));
            }
            ncbi_database.wait_for_completion();
            ASSERT_EQ(ncbi_database.get_ncbi_data(test_ncbi_id).geneid, test_expected_geneid);
         }
    }

    // Check function is_ncbi_match
    TEST(NCBIDatabase, CheckNCBIMatch) {
         // Ensure Uniprot matches return false
         ASSERT_FALSE(NCBIDatabase::is_ncbi_match("sp|Q92051|CAHZ_DANRE"));
         // Ensure invalid ID's return false
         ASSERT_FALSE(NCBIDatabase::is_ncbi_match("fdsafdsa"));
         // Ensure empty ID's return false
         ASSERT_FALSE(NCBIDatabase::is_ncbi_match(""));
         // Ensure NCBI ID's return true
         ASSERT_TRUE(NCBIDatabase::is_ncbi_match("XP_025418651.1"));
         ASSERT_TRUE(NCBIDatabase::is_ncbi_match("XP_025418651"));
         ASSERT_TRUE(NCBIDatabase::is_ncbi_match("XP_012251926.1"));
     }

}
#endif