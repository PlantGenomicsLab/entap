/*
*
 * Developed by Alexander Hart
 * Plant Computational Genomics Lab
 * University of Connecticut
 *
 * For information, contact Alexander Hart at:
 *     entap.dev@gmail.com
 *
 * Copyright 2017-2025, Alexander Hart, Dr. Jill Wegrzyn
 *
 * This file is part of EnTAP.
 *
 * EnTAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnTAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnTAP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef ENABLE_TESTING
#include "gtest/gtest.h"
#include "../../FileSystem.h"
#include "../../database/FastaDatabase.h"

namespace {
      TEST(FastaDatabase, NCBIRefseqDownload) {
          auto const startTime = std::chrono::system_clock::now();
          auto file_system = FileSystem(startTime);

          // Test downloading + configuring of NCBI RefSeq Archaea
          //    this is a smaller database and all of the other refseq fall through this same
          //    code space
          std::string out_path = file_system.get_temp_file();
          std::string out_err;
          auto fasta_database = FastaDatabase(FastaDatabase::FASTA_DATABASE_FILE_USER_CONFIG,
                                    FastaDatabase::FASTA_DATABASE_NCBI_PLASTID,
                                    "", &file_system);
          ASSERT_TRUE(fasta_database.download_configure_database(out_path, out_err));
          ASSERT_TRUE(file_system.file_exists(out_path));
          ASSERT_FALSE(file_system.file_empty(out_path));
          // read file and verify it contains sequences from different ncbi sources
          std::string line;
          std::ifstream file(out_path);
          bool test1=false, test2=false, test3=false, test4=false;
          std::string test1_str=">YP_010145810.1 ribulose-1,5-bisphosphate carboxylase/oxygenase large subunit (chloroplast) [Xylosma longifolia]";
          std::string test2_str=">YP_010834630.1 NdhF (chloroplast) [Cynanchum otophyllum]";
          std::string test3_str=">XP_022811199.1 apicoplast ribosomal protein S19 (apicoplast) [Plasmodium yoelii]";
          std::string test4_str="KARLKTLSSTVCKQGACTFGPFYRNKIIKYNKFIYKLIFLYLLINKRSNILIIKFEYILGLLYIYKNIKFNNINELIYKI";
          while (getline(file,line)) {
              if (line.empty()) continue;
              if (!test1) test1 = line.find(test1_str) != std::string::npos;
              if (!test2) test2 = line.find(test2_str) != std::string::npos;
              if (!test3) test3 = line.find(test3_str) != std::string::npos;
              if (!test4) test4 = line.find(test4_str) != std::string::npos;
          }
          ASSERT_TRUE(test1);
          ASSERT_TRUE(test2);
          ASSERT_TRUE(test3);
          ASSERT_TRUE(test4);
      }

      TEST(FastaDatabase, UniprotDownload) {
          auto const startTime = std::chrono::system_clock::now();
          auto file_system = FileSystem(startTime);

          // Test downloading + configuring of Uniprot swiss prot database
          //    this is a smaller database and trempl/NR fall under same code path
          std::string out_path = file_system.get_temp_file();
          std::string out_err;
          auto fasta_database = FastaDatabase(FastaDatabase::FASTA_DATABASE_FILE_USER_CONFIG,
                                    FastaDatabase::FASTA_DATABASE_UNIPROT_SPROT,
                                    "", &file_system);
          ASSERT_TRUE(fasta_database.download_configure_database(out_path, out_err));
          ASSERT_TRUE(file_system.file_exists(out_path));
          ASSERT_FALSE(file_system.file_empty(out_path));
          // read file and verify it contains sequences from different ncbi sources
          std::string line;
          std::ifstream file(out_path);
          bool test1=false, test2=false, test3=false, test4=false;
          std::string test1_str=">sp|Q197D8|022L_IIV3 Transmembrane protein 022L OS=Invertebrate iridescent virus 3 OX=345201 GN=IIV3-022L PE=4 SV=1";
          std::string test2_str=">sp|Q9SR24|P2C36_ARATH Probable protein phosphatase 2C 36 OS=Arabidopsis thaliana OX=3702 GN=PLL3 PE=3 SV=1";
          std::string test3_str=">sp|Q5HCX7|Y2589_STAAC UPF0291 protein SACOL2589 OS=Staphylococcus aureus (strain COL) OX=93062 GN=SACOL2589 PE=3 SV=1";
          std::string test4_str="LGTSGSLDFMFSWKSMFTITKKEGVDMDDLILELIDYGVEEEYDEDEDEITLYGDPKSFA";
          while (getline(file,line)) {
              if (line.empty()) continue;
              if (!test1) test1 = line.find(test1_str) != std::string::npos;
              if (!test2) test2 = line.find(test2_str) != std::string::npos;
              if (!test3) test3 = line.find(test3_str) != std::string::npos;
              if (!test4) test4 = line.find(test4_str) != std::string::npos;
          }
          ASSERT_TRUE(test1);
          ASSERT_TRUE(test2);
          ASSERT_TRUE(test3);
          ASSERT_TRUE(test4);
      }

      TEST(FastaDatabase, FastaUnknown) {
          auto const startTime = std::chrono::system_clock::now();
          auto file_system = FileSystem(startTime);
          std::string out_err;
          std::string out_path = file_system.get_temp_file();

          // Ensure unknown file type doesn't gather any data
          auto fasta_database = FastaDatabase(FastaDatabase::FASTA_DATABASE_FILE_UNKNOWN,
                                    FastaDatabase::FASTA_DATABASE_UNIPROT_SPROT,
                                    "", &file_system);
          ASSERT_FALSE(fasta_database.download_configure_database(out_path, out_err));
          ASSERT_FALSE(file_system.file_exists(out_path));

          // Ensure unknown database type doesn't gather any data
          // Ensure unknown file type doesn't gather any data
          fasta_database = FastaDatabase(FastaDatabase::FASTA_DATABASE_FILE_USER_CONFIG,
                                    FastaDatabase::FASTA_DATABASE_UNKNOWN,
                                    "", &file_system);
          ASSERT_FALSE(fasta_database.download_configure_database(out_path, out_err));
          ASSERT_FALSE(file_system.file_exists(out_path));

          // Ensure data not gathered for any other database file type for now
          fasta_database = FastaDatabase(FastaDatabase::FASTA_DATABASE_FILE_TEXT,
                                    FastaDatabase::FASTA_DATABASE_NCBI_ARCHAEA,
                                    "", &file_system);
          ASSERT_FALSE(fasta_database.download_configure_database(out_path, out_err));
          ASSERT_FALSE(file_system.file_exists(out_path));

          // Ensure data not gathered when empty outpath is tried
          fasta_database = FastaDatabase(FastaDatabase::FASTA_DATABASE_FILE_USER_CONFIG,
                                    FastaDatabase::FASTA_DATABASE_NCBI_ARCHAEA,
                                    "", &file_system);
          ASSERT_FALSE(fasta_database.download_configure_database("", out_err));
      }

}

#endif