/*
*
 * Developed by Alexander Hart
 * Plant Computational Genomics Lab
 * University of Connecticut
 *
 * For information, contact Alexander Hart at:
 *     entap.dev@gmail.com
 *
 * Copyright 2017-2025, Alexander Hart, Dr. Jill Wegrzyn
 *
 * This file is part of EnTAP.
 *
 * EnTAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnTAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnTAP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef ENABLE_TESTING
#include "gtest/gtest.h"
#include "../../database/EntapDatabase.h"
#include "../../FileSystem.h"

namespace {

    class EntapDatabaseTest : public testing::Test {
    protected:

        EntapDatabaseTest(): mpFilesystem(nullptr), mpEntapDatabase(nullptr) {
        }

        ~EntapDatabaseTest() override = default;

        void SetUp() override {
            auto const startTime = std::chrono::system_clock::now();
            mpFilesystem = new FileSystem(startTime);
            std::string root = FileSystem::get_cur_dir();
            mpFilesystem->set_root_dir(root);
            mpEntapDatabase = new EntapDatabase(mpFilesystem);
        }

        void TearDown() override {
            delete mpEntapDatabase;
            delete mpFilesystem;
        }

        FileSystem* mpFilesystem;
        EntapDatabase* mpEntapDatabase;

        void gene_ontology_entry_tests() const {
            std::string go_test;
            GoEntry goEntry;

            go_test = "GO:0000166";
            goEntry = mpEntapDatabase->get_go_entry(go_test);
            ASSERT_EQ(goEntry.category, "molecular_function");
            ASSERT_EQ(goEntry.go_id, "GO:0000166");
            ASSERT_EQ(goEntry.term, "nucleotide binding");

            go_test = "GO:0007275";
            goEntry = mpEntapDatabase->get_go_entry(go_test);
            ASSERT_EQ(goEntry.category, "biological_process");
            ASSERT_EQ(goEntry.go_id, "GO:0007275");
            ASSERT_EQ(goEntry.term, "multicellular organism development");

            go_test = "GO:0019717";
            goEntry = mpEntapDatabase->get_go_entry(go_test);
            ASSERT_EQ(goEntry.category,"cellular_component");
            ASSERT_EQ(goEntry.go_id, "GO:0019717");
            ASSERT_EQ(goEntry.term, "obsolete synaptosome");
        }

        void taxonomy_entry_tests() const {
            TaxEntry taxEntry;
            std::string tax_test;

            tax_test = "pinus";
            taxEntry = mpEntapDatabase->get_tax_entry(tax_test);
            ASSERT_EQ(taxEntry.tax_name, "pinus");
            ASSERT_EQ(taxEntry.lineage, "pinus;pinaceae;pinales;conifers i;pinidae;pinopsida;acrogymnospermae;" \
                                       "spermatophyta;euphyllophyta;tracheophyta;embryophyta;streptophytina;"\
                                       "streptophyta;viridiplantae;eukaryota;cellular organisms;root");
            ASSERT_EQ(taxEntry.tax_id, "3337");

            tax_test = "homo sapiens";
            taxEntry = mpEntapDatabase->get_tax_entry(tax_test);
            ASSERT_EQ(taxEntry.tax_name, "homo sapiens");
            ASSERT_EQ(taxEntry.lineage, "homo sapiens;homo;homininae;hominidae;hominoidea;catarrhini;simiiformes;"\
                                       "haplorrhini;primates;euarchontoglires;boreoeutheria;eutheria;theria;"\
                                       "mammalia;amniota;tetrapoda;dipnotetrapodomorpha;sarcopterygii;"\
                                       "euteleostomi;teleostomi;gnathostomata;vertebrata;craniata;chordata;"\
                                       "deuterostomia;bilateria;eumetazoa;metazoa;opisthokonta;eukaryota;"\
                                       "cellular organisms;root");
            ASSERT_EQ(taxEntry.tax_id, "9606");
        }

        void uniprot_entry_tests() const {
            UniprotEntry uniprotEntry;
            std::string uniprot_test;

            uniprot_test = "MK67I_BOVIN";
            uniprotEntry = mpEntapDatabase->get_uniprot_entry(uniprot_test);
            ASSERT_EQ(uniprotEntry.uniprot_id, "MK67I_BOVIN");
            ASSERT_EQ(uniprotEntry.go_terms.size(), 6);
            for (const GoEntry &entry : uniprotEntry.go_terms) {
                ASSERT_TRUE(entry.go_id == "GO:0003723" || entry.go_id == "GO:0000463" || entry.go_id == "GO:0005654" ||
                       entry.go_id == "GO:0005730" || entry.go_id == "GO:0005737" || entry.go_id == "GO:0000794");
            }

            uniprot_test = "ANFY1_MOUSE";
            uniprotEntry = mpEntapDatabase->get_uniprot_entry(uniprot_test);
            ASSERT_EQ(uniprotEntry.uniprot_id, "ANFY1_MOUSE");
            ASSERT_EQ(uniprotEntry.go_terms.size(), 13);
            /*  DR   GO; GO:0005829; C:cytosol; IEA:GOC.
                DR   GO; GO:0005769; C:early endosome; IEA:Ensembl.
                DR   GO; GO:0010008; C:endosome membrane; IDA:UniProtKB.
                DR   GO; GO:0044354; C:macropinosome; IEA:Ensembl.
                DR   GO; GO:0030904; C:retromer complex; IEA:Ensembl.
                DR   GO; GO:0046872; F:metal ion binding; IEA:UniProtKB-KW.
                DR   GO; GO:1901981; F:phosphatidylinositol phosphate binding; IEA:Ensembl.
                DR   GO; GO:0031267; F:small GTPase binding; IEA:Ensembl.
                DR   GO; GO:0006897; P:endocytosis; NAS:UniProtKB.
                DR   GO; GO:0034058; P:endosomal vesicle fusion; IEA:Ensembl.
                DR   GO; GO:0090160; P:Golgi to lysosome transport; IEA:Ensembl.
                DR   GO; GO:0048549; P:positive regulation of pinocytosis; IEA:Ensembl.
                DR   GO; GO:0042147; P:retrograde transport, endosome to Golgi; IEA:Ensembl.
             */
            for (const GoEntry &entry : uniprotEntry.go_terms) {
                ASSERT_TRUE(entry.go_id == "GO:0005829" || entry.go_id == "GO:0005769" ||
                       entry.go_id == "GO:0010008" || entry.go_id == "GO:0044354" ||
                       entry.go_id == "GO:0030904" || entry.go_id == "GO:0046872" ||
                       entry.go_id == "GO:1901981" || entry.go_id == "GO:0031267" || entry.go_id == "GO:0006897" ||
                       entry.go_id == "GO:0034058" || entry.go_id == "GO:0090160" ||
                       entry.go_id == "GO:0048549" || entry.go_id == "GO:0042147");
            }

            // database xref test
            uniprot_test = "PLAP_RAT";
            uniprotEntry = mpEntapDatabase->get_uniprot_entry(uniprot_test);
            ASSERT_EQ(uniprotEntry.uniprot_id, uniprot_test);
            ASSERT_EQ(uniprotEntry.db_x_refs_orthodb, "1116432at2759;");
            ASSERT_EQ(uniprotEntry.db_x_refs_interpro, "IPR011989(ARM-like);IPR015155(PFU);IPR038122(PFU_sf);"
                                                       "IPR013535(PUL_dom);IPR015943(WD40/YVTN_repeat-like_dom_sf);"
                                                       "IPR036322(WD40_repeat_dom_sf);IPR001680(WD40_rpt);");
            ASSERT_EQ(uniprotEntry.db_x_refs_pfam, "PF09070(PFU);PF08324(PUL);PF00400(WD40);");

            // database xref test
            uniprot_test = "5NTC_HUMAN";
            uniprotEntry = mpEntapDatabase->get_uniprot_entry(uniprot_test);
            uniprotEntry.db_x_refs_orthodb = "3626840at2759;";
            uniprotEntry.db_x_refs_pfam = "PF05761(5_nucleotid);";
            uniprotEntry.db_x_refs_interpro = "IPR036412(HAD-like_sf);IPR008380(HAD-SF_hydro_IG_5-nucl);"
                                              "IPR023214(HAD_sf);IPR016695(Pur_nucleotidase);";

            // empty data
            uniprot_test = "fdjsakl;fjdsa";
            uniprotEntry = {};
            uniprotEntry = mpEntapDatabase->get_uniprot_entry(uniprot_test);
            ASSERT_TRUE(uniprotEntry.is_empty());
            ASSERT_TRUE(uniprotEntry.comments.empty());
            ASSERT_TRUE(uniprotEntry.database_x_refs.empty());
            ASSERT_TRUE(uniprotEntry.go_terms.empty());
        }

        void pfam_entry_tests() const {
            PfamEntry pfam_entry;
            std::string pfam_test;
            // Random pfam entry
            pfam_entry = {};
            pfam_test = "PK";
            pfam_entry = mpEntapDatabase->get_pfam_entry(pfam_test);
            ASSERT_EQ(pfam_entry.pfam_accession_id,"PF00224");
            ASSERT_EQ(pfam_entry.pfam_name, pfam_test);
            ASSERT_EQ(pfam_entry.pfam_description, "Pyruvate kinase, barrel domain");

            // Random pfam entry
            pfam_entry = {};
            pfam_test = "Ldh_1_C";
            pfam_entry = mpEntapDatabase->get_pfam_entry(pfam_test);
            ASSERT_EQ(pfam_entry.pfam_accession_id, "PF02866");
            ASSERT_EQ(pfam_entry.pfam_name, pfam_test);
            ASSERT_EQ(pfam_entry.pfam_description, "lactate/malate dehydrogenase, alpha/beta C-terminal domain");

            // Random pfam entry
            pfam_entry = {};
            pfam_test = "ZZ";
            pfam_entry = mpEntapDatabase->get_pfam_entry(pfam_test);
            ASSERT_EQ(pfam_entry.pfam_accession_id, "PF00569");
            ASSERT_EQ(pfam_entry.pfam_name, pfam_test);
            ASSERT_EQ(pfam_entry.pfam_description, "Zinc finger, ZZ type");

            // Unknown entry returns empty data
            pfam_entry = {};
            pfam_test = "asdfgh";
            pfam_entry = mpEntapDatabase->get_pfam_entry(pfam_test);
            ASSERT_TRUE(pfam_entry.pfam_accession_id.empty());
            ASSERT_TRUE(pfam_entry.pfam_name.empty());
            ASSERT_TRUE(pfam_entry.pfam_description.empty());
        }
    };

    TEST_F(EntapDatabaseTest, GeneOntologySQL) {
        EntapDatabase::DATABASE_ERR databaseErr;

        std::string test_db_path = PATHS(mpFilesystem->get_temp_outdir(), "database_temp.db");

        databaseErr = mpEntapDatabase->create_database_type(EntapDatabase::ENTAP_SQL,test_db_path);
        ASSERT_EQ(databaseErr, EntapDatabase::ERR_DATA_OK);

        databaseErr = mpEntapDatabase->generate_entap_go(EntapDatabase::ENTAP_SQL);
        ASSERT_EQ(databaseErr, EntapDatabase::ERR_DATA_OK);

        gene_ontology_entry_tests();
    }

    TEST_F(EntapDatabaseTest, GeneOntologySerial) {
        EntapDatabase::DATABASE_ERR databaseErr;

        std::string test_db_path = PATHS(mpFilesystem->get_temp_outdir(), "database_temp.db");

        databaseErr = mpEntapDatabase->create_database_type(EntapDatabase::ENTAP_SERIALIZED,test_db_path);
        ASSERT_EQ(databaseErr, EntapDatabase::ERR_DATA_OK);

        databaseErr = mpEntapDatabase->generate_entap_go(EntapDatabase::ENTAP_SERIALIZED);
        ASSERT_EQ(databaseErr, EntapDatabase::ERR_DATA_OK);

        gene_ontology_entry_tests();
    }

    TEST_F(EntapDatabaseTest, NCBITaxonomySQL) {
        EntapDatabase::DATABASE_ERR databaseErr;

        std::string test_db_path = PATHS(mpFilesystem->get_temp_outdir(), "database_temp.db");

        databaseErr = mpEntapDatabase->create_database_type(EntapDatabase::ENTAP_SQL,test_db_path);
        ASSERT_EQ(databaseErr, EntapDatabase::ERR_DATA_OK);

        databaseErr = mpEntapDatabase->generate_entap_tax(EntapDatabase::ENTAP_SQL);
        ASSERT_EQ(databaseErr, EntapDatabase::ERR_DATA_OK);

        taxonomy_entry_tests();
    }

    TEST_F(EntapDatabaseTest, NCBITaxonomySerial) {
        EntapDatabase::DATABASE_ERR databaseErr;

        std::string test_db_path = PATHS(mpFilesystem->get_temp_outdir(), "database_temp.db");

        databaseErr = mpEntapDatabase->create_database_type(EntapDatabase::ENTAP_SERIALIZED,test_db_path);
        ASSERT_EQ(databaseErr, EntapDatabase::ERR_DATA_OK);

        databaseErr = mpEntapDatabase->generate_entap_tax(EntapDatabase::ENTAP_SERIALIZED);
        ASSERT_EQ(databaseErr, EntapDatabase::ERR_DATA_OK);

        taxonomy_entry_tests();
    }

    TEST_F(EntapDatabaseTest, UniprotSQL) {
        EntapDatabase::DATABASE_ERR databaseErr;

        std::string test_db_path = PATHS(mpFilesystem->get_temp_outdir(), "database_temp.db");

        databaseErr = mpEntapDatabase->create_database_type(EntapDatabase::ENTAP_SQL,test_db_path);
        ASSERT_EQ(databaseErr, EntapDatabase::ERR_DATA_OK);

        // uniprot sql relies on the gene ontology database being generated since terms are pulled from there
        databaseErr = mpEntapDatabase->generate_entap_go(EntapDatabase::ENTAP_SQL);
        ASSERT_EQ(databaseErr, EntapDatabase::ERR_DATA_OK);

        databaseErr = mpEntapDatabase->generate_entap_uniprot(EntapDatabase::ENTAP_SQL);
        ASSERT_EQ(databaseErr, EntapDatabase::ERR_DATA_OK);

        uniprot_entry_tests();
    }

    TEST_F(EntapDatabaseTest, UniprotSerial) {
        EntapDatabase::DATABASE_ERR databaseErr;

        std::string test_db_path = PATHS(mpFilesystem->get_temp_outdir(), "database_temp.db");

        databaseErr = mpEntapDatabase->create_database_type(EntapDatabase::ENTAP_SERIALIZED,test_db_path);
        ASSERT_EQ(databaseErr, EntapDatabase::ERR_DATA_OK);

        databaseErr = mpEntapDatabase->generate_entap_uniprot(EntapDatabase::ENTAP_SERIALIZED);
        ASSERT_EQ(databaseErr, EntapDatabase::ERR_DATA_OK);

        uniprot_entry_tests();
    }

    TEST_F(EntapDatabaseTest, APITaxonomy) {
        std::string taxon;

        taxon = "homo sapiens";
        ASSERT_TRUE(mpEntapDatabase->is_ncbi_tax_entry(taxon));

        taxon = "homo_sapiens";
        ASSERT_TRUE(mpEntapDatabase->is_ncbi_tax_entry(taxon));

        taxon = "hfdasfa";
        ASSERT_FALSE(mpEntapDatabase->is_ncbi_tax_entry(taxon));
    }

    TEST_F(EntapDatabaseTest, PFAMSerial) {
        EntapDatabase::DATABASE_ERR databaseErr;
        PfamEntry pfam_entry;
        std::string pfam_test;

        std::string test_db_path = PATHS(mpFilesystem->get_temp_outdir(), "database_temp.db");

        databaseErr = mpEntapDatabase->create_database_type(EntapDatabase::ENTAP_SERIALIZED,test_db_path);
        ASSERT_EQ(databaseErr, EntapDatabase::ERR_DATA_OK);

        databaseErr = mpEntapDatabase->generate_entap_pfam(EntapDatabase::ENTAP_SERIALIZED);
        ASSERT_EQ(databaseErr, EntapDatabase::ERR_DATA_OK);

        pfam_entry_tests();
    }

    TEST_F(EntapDatabaseTest, PFAMSQL) {
        EntapDatabase::DATABASE_ERR databaseErr;
        PfamEntry pfam_entry;
        std::string pfam_test;

        std::string test_db_path = PATHS(mpFilesystem->get_temp_outdir(), "database_temp.db");

        databaseErr = mpEntapDatabase->create_database_type(EntapDatabase::ENTAP_SQL,test_db_path);
        ASSERT_EQ(databaseErr, EntapDatabase::ERR_DATA_OK);

        databaseErr = mpEntapDatabase->generate_entap_pfam(EntapDatabase::ENTAP_SQL);
        ASSERT_EQ(databaseErr, EntapDatabase::ERR_DATA_OK);

        pfam_entry_tests();
    }

    TEST_F(EntapDatabaseTest, GenerateReadSerial) {
        std::string test_db_path = PATHS(mpFilesystem->get_m_test_data_dir(), "database_temp.bin");

        // Generate entire entap database
        EntapDatabase::DATABASE_ERR databaseErr = mpEntapDatabase->generate_database(EntapDatabase::ENTAP_SERIALIZED, test_db_path);
        ASSERT_EQ(databaseErr, EntapDatabase::ERR_DATA_OK);

        // reset data structures
        delete mpEntapDatabase;
        mpEntapDatabase = new EntapDatabase(mpFilesystem);

        // Read database
        ASSERT_TRUE(mpEntapDatabase->set_database(EntapDatabase::ENTAP_SERIALIZED, test_db_path));

        // Verify entry data
        gene_ontology_entry_tests();
        taxonomy_entry_tests();
        uniprot_entry_tests();
        pfam_entry_tests();
    }

    TEST_F(EntapDatabaseTest, GenerateReadSQL) {
        std::string test_db_path = PATHS(mpFilesystem->get_m_test_data_dir(), "database_temp.sql");

        // Generate entire entap database
        EntapDatabase::DATABASE_ERR databaseErr = mpEntapDatabase->generate_database(EntapDatabase::ENTAP_SQL, test_db_path);
        ASSERT_EQ(databaseErr, EntapDatabase::ERR_DATA_OK);

        // reset data structures
        delete mpEntapDatabase;
        mpEntapDatabase = new EntapDatabase(mpFilesystem);

        // Read database
        ASSERT_TRUE(mpEntapDatabase->set_database(EntapDatabase::ENTAP_SQL, test_db_path));

        // Verify entry data
        gene_ontology_entry_tests();
        taxonomy_entry_tests();
        uniprot_entry_tests();
        pfam_entry_tests();
    }

}  // namespace
#endif