/******************************************************************
*
 * Developed by Alexander Hart
 * Plant Computational Genomics Lab
 * University of Connecticut
 *
 * For information, contact Alexander Hart at:
 *     entap.dev@gmail.com
 *
 * Copyright 2017-2025, Alexander Hart, Dr. Jill Wegrzyn
 *
 * This file is part of EnTAP.
 *
 * EnTAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EnTAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EnTAP.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************/

#ifdef ENABLE_TESTING
#include "gtest/gtest.h"
#include "../../database/NCBIEntrez.h"

    TEST(NCBIEntrez, NCBIProteinFetch) {
        auto const startTime = std::chrono::system_clock::now();
        auto *pfilesystem = new FileSystem(startTime);
        // std::string root = mpFilesystem->get_cur_dir();
        // mpFilesystem->set_root_dir(root);
        NCBIEntrez ncbi_entrez(pfilesystem,"");

        // Test 1, pulling gene id from protein database for a single NCBI ID
        std::string test_ncbi_id = "XP_014245616";
        std::string test_expected_geneid = "106664428";
        NCBIEntrez::EntrezResults entrez_results;
        NCBIEntrez::EntrezInput entrez_input;
        entrez_input.data_types = {NCBIEntrez::ENTREZ_DATA_GENEID};
        entrez_input.uid_list = {test_ncbi_id};
        entrez_input.database = NCBIEntrez::NCBI_DATABASE_PROTEIN;
        entrez_input.rettype = NCBIEntrez::NCBI_ENTREZ_RETTYPE_GP;

        ncbi_entrez.entrez_fetch(entrez_input, entrez_results);
        EXPECT_TRUE(!entrez_results.entrez_results.empty());
        EXPECT_TRUE(entrez_results.entrez_results.find(test_ncbi_id) != entrez_results.entrez_results.end());
        EXPECT_EQ(entrez_results.entrez_results.at(test_ncbi_id).geneid, test_expected_geneid);


        // Test 2, pulling gene id from protein database for multiple NCBI ID
        //  including NCBI versions (XXXX.1)
        entrez_input = {};
        entrez_results = {};
        entrez_input.data_types = {NCBIEntrez::ENTREZ_DATA_GENEID};
        entrez_input.uid_list = {"XP_014245616", "XP_020482136.1", "XP_026476231.1"};
        entrez_input.database = NCBIEntrez::NCBI_DATABASE_PROTEIN;
        entrez_input.rettype = NCBIEntrez::NCBI_ENTREZ_RETTYPE_GP;

        ncbi_entrez.entrez_fetch(entrez_input, entrez_results);
        EXPECT_TRUE(!entrez_results.entrez_results.empty());
        EXPECT_TRUE(entrez_results.entrez_results.find("XP_014245616") != entrez_results.entrez_results.end());
        EXPECT_TRUE(entrez_results.entrez_results.at("XP_014245616").geneid == "106664428");
        EXPECT_TRUE(entrez_results.entrez_results.find("XP_020482136.1") != entrez_results.entrez_results.end());
        EXPECT_TRUE(entrez_results.entrez_results.at("XP_020482136.1").geneid == "109976342");
        EXPECT_TRUE(entrez_results.entrez_results.find("XP_026476231.1") != entrez_results.entrez_results.end());
        EXPECT_TRUE(entrez_results.entrez_results.at("XP_026476231.1").geneid == "113381705");

        // Test 3, entrez_fetch returns FALSE if no data retrieved
        entrez_input = {};
        entrez_results = {};
        entrez_input.data_types = {NCBIEntrez::ENTREZ_DATA_GENEID};
        entrez_input.uid_list = {"XP_014245616safdsafs"};
        entrez_input.database = NCBIEntrez::NCBI_DATABASE_PROTEIN;
        entrez_input.rettype = NCBIEntrez::NCBI_ENTREZ_RETTYPE_GP;

        EXPECT_FALSE(ncbi_entrez.entrez_fetch(entrez_input, entrez_results));

        // Test 4, entrez_fetch returns FALSE with no UID list
        entrez_input = {};
        entrez_results = {};
        entrez_input.data_types = {NCBIEntrez::ENTREZ_DATA_GENEID};
        entrez_input.uid_list = {};
        entrez_input.database = NCBIEntrez::NCBI_DATABASE_PROTEIN;
        entrez_input.rettype = NCBIEntrez::NCBI_ENTREZ_RETTYPE_GP;

        EXPECT_FALSE(ncbi_entrez.entrez_fetch(entrez_input, entrez_results));
   }

    // Access to NCBI Entrez API is limited to 3 accessions per second
    //  unless using API key which allows for 10 accessions per second
    // https://www.ncbi.nlm.nih.gov/books/NBK25497/
    TEST(NCBIEntrez, APIAccessLimit) {
        uint32 NCBI_API_ACCESS_TIMEOUT = 1000;
        uint32 NCBI_API_ACCESSION_PER_TIMEOUT = 3;
        auto const startTime = std::chrono::system_clock::now();
        auto *pfilesystem = new FileSystem(startTime);
        NCBIEntrez ncbi_entrez(pfilesystem,"");
        std::string test_ncbi_id = "XP_014245616";
        std::string test_expected_geneid = "106664428";
        NCBIEntrez::EntrezResults entrez_results;
        NCBIEntrez::EntrezInput entrez_input;
        entrez_input.data_types = {NCBIEntrez::ENTREZ_DATA_GENEID};
        entrez_input.uid_list = {test_ncbi_id};
        entrez_input.database = NCBIEntrez::NCBI_DATABASE_PROTEIN;
        entrez_input.rettype = NCBIEntrez::NCBI_ENTREZ_RETTYPE_GP;
        uint32 accessions_ct = 0;

        // Simulating multiple fetch accessions at once, checking on search completion
        //  verifying data successfully pulled. Note, since we're verifying after search
        //  has comleted here, the API access timing is unlikely to fail
        auto start_timer = std::chrono::high_resolution_clock::now();
        for (int i = 0; i < 10; i++) {
            ASSERT_TRUE(ncbi_entrez.entrez_fetch(entrez_input, entrez_results));
            // Check if this finished since a previous fetch
            if (++accessions_ct >= NCBI_API_ACCESSION_PER_TIMEOUT) {
                auto end_timer = std::chrono::steady_clock::now();
                auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end_timer - start_timer).count();
                ASSERT_TRUE(duration >= NCBI_API_ACCESS_TIMEOUT);
                start_timer = std::chrono::high_resolution_clock::now();
                accessions_ct =0;
            }
            ASSERT_TRUE(!entrez_results.entrez_results.empty());
            ASSERT_TRUE(entrez_results.entrez_results.find(test_ncbi_id) != entrez_results.entrez_results.end());
            ASSERT_EQ(entrez_results.entrez_results.at(test_ncbi_id).geneid, test_expected_geneid);
            entrez_results = {};
        }
        // Sleep to not interefere with next test
        std::this_thread::sleep_for(std::chrono::milliseconds(NCBI_API_ACCESS_TIMEOUT*3));

        // Simulate when API acccess is allowed, this should more realistically mimic
        //  real life conditions. agnostic of download speeds
        accessions_ct = 0;
        auto cycle_time = std::chrono::system_clock::now();
        for (int i = 0; i < 100; i++) {
            ASSERT_TRUE(ncbi_entrez.allowed_to_access_api());
            // Check if this finished since a previous fetch
            if (++accessions_ct > NCBI_API_ACCESSION_PER_TIMEOUT) {
                auto cur_time = std::chrono::system_clock::now();
                auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(cur_time - cycle_time).count();
                ASSERT_TRUE(duration > NCBI_API_ACCESS_TIMEOUT);
                accessions_ct =0;
                cycle_time = cur_time;
            }
        }

    }

#endif